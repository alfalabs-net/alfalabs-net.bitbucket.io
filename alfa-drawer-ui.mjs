/** makes left and right animated drawer (for menus)
 * usage:
 * 
 * enter this html element into <body> tag as last element on page
 
        <div id="alfa-drawer-wrapper"></div>

* script
            const AlfaDrawer = require('../../Alfa_LIB/alfa-drawer')
            var alfaDrawerLeft =  new AlfaDrawer('left')
 */
'use strict';

const $dqs = document.querySelector.bind(document); 
const $dce = document.createElement.bind(document); 


function AlfaDrawer(drawerSide, opts) {

    opts = opts || {}
    this.transitionTime =   opts.transitionTime || 400; //ms
    this.edgeTriggerDebug = opts.edgeTriggerDebug || false; // show mouseover trigger areas at the edges of screen
    this.noOverlay =        opts.noOverlay || false;
    this.noEdgeTrigger =    opts.noEdgeTrigger || false;
    this.noCloseBtn =       opts.noCloseBtn || false;
    this.edgeTriggeredDelay = opts.edgeTriggeredDelay || 2000
    this.size =             opts.size || (drawerSide==='left' | drawerSide==='right' ? '20%' : '10%');

    this.wrapper =  $dqs('#alfa-drawer-wrapper');
    if(!this.wrapper){console.error('[alfa-drawer-ui.mjs]', drawerSide, '#alfa-drawer-wrapper not found!'); return}

    this.drawerSide = drawerSide;

    this.build_ui();
    
    this.insertStyleTag();
}
AlfaDrawer.prototype.build_ui = function(){
    /** 1. make trigger */
    if(!this.noEdgeTrigger ){
        this.drawerTrigger = $dce('div')
        this.drawerTrigger.classList.add(`alfa-drawer-${this.drawerSide}-trigger`)
        this.wrapper.append(this.drawerTrigger);
        this.drawerTrigger.addEventListener('mouseenter', this.edgeTriggerHover.bind(this)); //mousemove mouseover mouseenter
    }
 
    /** 2. make overlay */
    if(!this.noOverlay){
        /** 2a. check if overlay is already set by other drawer */
        this.overlay = this.wrapper.querySelector('.alfa-drawer-overlay')
        if (!this.overlay){
            this.overlay = $dce('div')
            this.overlay.classList.add('alfa-drawer-overlay');
            this.wrapper.append(this.overlay)
        }
        this.overlay.addEventListener('click', this.overlayClk.bind(this));
    }

    /** 3. make drawer */
    this.drawer = $dce('div');
    this.drawer.classList.add(`alfa-drawer-${this.drawerSide}`);
    this.wrapper.append(this.drawer)

    /** close drawer after doing nothing inside drawer - needed mouse position */
    $dqs(`.alfa-drawer-${this.drawerSide}`).addEventListener('mouseout', this.mouseOutHandler.bind(this))

    /** 4. make close btn */
    if(this.noOverlay){
        /** make button, because there is no overlay click to close */
        this.makeCloseBtn();
    } else {
        if (!this.noCloseBtn) this.makeCloseBtn();
    }

    /** 5. load content by moving it */
    var target = this.wrapper.querySelector('.alfa-drawer-'+this.drawerSide)
    var source = this.wrapper.querySelector('.alfa-drawer-content.'+this.drawerSide)
    if(!source){console.error('[alfa-drawer-ui]',this.drawerSide, 'drawer content is missing'); return}

    target.append(source)

    source.style.display = 'block'

}

/** to close drawer after inactivity, but not when user has mouse in drawer */
AlfaDrawer.prototype.mouseOutHandler = function(ev){
    if(ev.target.classList.contains(`alfa-drawer-${this.drawerSide}`)){
        var containerSize, mousePos
        switch(this.drawerSide){
            case 'top':
            case 'bottom':
                containerSize = ev.target.offsetHeight
                mousePos = ev.clientY;      break
            case 'left':
            case 'right':
                containerSize = ev.target.offsetWidth  
                mousePos = ev.clientX;      break  
        }
        this.isMouseIn = !(mousePos > containerSize)
    }
    // ev.target.removeEventListener('mouseout', this.mouseOutHandler) - removing not working, event is declared once
}

AlfaDrawer.prototype.open = function(opts){

    if( this.status === 'opening' || this.status === 'opened') {console.warn('[alfa-drawer-menu-ui] is'+ this.status +' and attempting to open'); return;}

    this.overlayOn();
    this.drawer.style.display = 'block';
    this.edgeTriggers('off');

    this.status = 'opening'
    setTimeout(function(){ /** transition needs delay after display=block  */
        this.drawer.style[this.drawerSide] = 0;
        this.status = 'opened'
    }.bind(this), 0);

    this.avoidScrollbars()

        /** workaround - 'transitionend' event does not work in Chrome [Issue 868224] */
        // setTimeout(function(){        }.bind(this), this.transitionTime + 10)
}
AlfaDrawer.prototype.close = function(){

    if( this.status === 'closing' || this.status === 'closed') {console.warn('[alfa-drawer-menu-ui] is'+ this.status +' and attempting to close'); return;}

    this.status = 'closing'

    /** workaround - 'transitionend' event does not work in Chrome [Issue 868224] */
    setTimeout(function(){
        if(this.overlay) this.overlay.style.display = 'none';
        this.drawer.style.display = 'none';
        this.edgeTriggers('on');
        this.isMouseIn = false
        this.status = 'closed'
    }.bind(this), this.transitionTime + 10)
    
    switch(this.drawerSide){
        case 'left':   
        case 'right':  
            var width = this.drawer.offsetWidth;
            this.drawer.style[this.drawerSide] = `-${width}px`;
            break;
        case 'top':    
        case 'bottom': 
            var height = this.drawer.offsetHeight;
            this.drawer.style[this.drawerSide] = `-${height}px`;
            break;
    }
    this.avoidScrollbars()
}
// AlfaDrawer.prototype.onCloseAnimationEnd = function(){
//     /** since 'transitionended' event is not working in Chrome, [Issue 868224]
//      *  here is the workaround */
//     setTimeout(function(){
//         if(this.overlay) this.overlay.style.display = 'none';
//         this.drawer.style.display = 'none';
//         this.edgeTriggers('on');
//     }.bind(this), this.transitionTime + 10)
// }

/** not firing for some reason - not working in Chrome [Issue 868224] */
// AlfaDrawer.prototype.transitionCloseEnded = function(ev){
//     console.log('transitionCloseEnded')
//     if(this.overlay) this.overlay.style.display = 'none';
//     this.drawer.style.display = 'none';
//     this.edgeTriggers('on');
//     this.drawer.removeEventListener('transitionend', this.transitionCloseEnded)
// },

AlfaDrawer.prototype.overlayOn = function(){
    if(this.noOverlay){return;}
    
    this.overlay.style.display = 'block';
    this.overlay.setAttribute('side', this.drawerSide);

    setTimeout(function(){ /** after overlay is set to block, needs some time before transition can run */
        this.overlay.style.opacity = 0.1;
    }.bind(this), 0)
}

/** click on overlay to close drawer */
AlfaDrawer.prototype.overlayClk = function(ev){
    var side = ev.target.getAttribute('side');
    if(side!==this.drawerSide) return;
    // console.log('overlayClk', side, this.drawerSide)
    this.close();
    this.overlay.style.opacity = '0';
}

/** mouse over on screen edges to open edge drawer */
AlfaDrawer.prototype.edgeTriggerHover = function(ev){
    this.open();
    
    /** when opened by edgeTrigger, close after dealy */
    if(this.edgeTriggeredDelay){
        this.isMouseIn = true   /** mouseOutHandler() sets it to false */
        closeAfterInactivity.call(this)
    }

    function closeAfterInactivity(){
        setTimeout(function(){
            if(!this.isMouseIn) this.close()
            else closeAfterInactivity.call(this)
        }.bind(this), 
        this.edgeTriggeredDelay
        )
    }

}
AlfaDrawer.prototype.edgeTriggers = function(mode){
    /** find all edge triggers for drawers */
    var triggers = this.wrapper.querySelectorAll('[class*="trigger"]')
    if(mode==='off'){
        triggers.forEach(function(elem){
            elem.style.display = 'none';
        });
    } else {
        /** do not trigger edge triggers while clicking items in drawer close to the edge */
        setTimeout(function(){
            triggers.forEach(function(elem){
                elem.style.display = 'block';
            });
        }, 500)
    }
}

/** avoid scrollbars flashing in window, when right drawer is animated */
AlfaDrawer.prototype.avoidScrollbars = function(){
    if(this.drawerSide==='right' | this.drawerSide==='bottom'){
        var outsideContainer = this.wrapper.parentElement;
        outsideContainer.style.overflow = 'hidden';
        setTimeout(function(){
            outsideContainer.style.overflow = 'unset';
        }.bind(this), this.transitionTime + 10)
    }
}

AlfaDrawer.prototype.makeCloseBtn = function(){
    this.btnClose = $dce('div')
    this.btnClose.classList.add(`alfa-drawer-${this.drawerSide}-close`);

    switch(this.drawerSide){
        case 'left':   this.btnClose.innerHTML = '&#x1f878;'; break;
        case 'right':  this.btnClose.innerHTML = '&#x1f87a;'; break;
        case 'top':    this.btnClose.innerHTML = '&#x1f879;'; break;
        case 'bottom': this.btnClose.innerHTML = '&#x1F87B;'; break;
    }
   
    this.drawer.append(this.btnClose);
    this.btnClose.addEventListener('click', this.close.bind(this))
}

// AaaTemplateUI.prototype.clearNode = function(myNode){
//     while (myNode.firstChild) {
//         myNode.removeChild(myNode.lastChild);
//     }
// }

/** CSS */
AlfaDrawer.prototype.insertStyleTag = function(){

    var styleTagAttr = `alfa-drawer-${this.drawerSide}`;

    /** do not duplicate <style> tags */
    if ($dqs(`style[${this.styleTagAttr}]`)) return;

    var styleTag = $dce('style');
    var cssParent = '#alfa-drawer-wrapper';


    /** assign color to mouseover edge trigger areas */
    var triggerDebugCss = this.edgeTriggerDebug ? 'background: lime;opacity: .1;' : ''

    switch(this.drawerSide){
        case 'left':   
            styleTag.innerHTML = `
            ${cssParent} .alfa-drawer-left{

                --width: ${this.size};
        
                display: none;
                width: var(--width);
                height: 100%;
                background: white;;
                position: absolute; 
                top: 0;
        
                left: calc(var(--width) * -1);
                transition: left ${this.transitionTime}ms ease-in-out;
            }   
            ${cssParent} .alfa-drawer-left-trigger{
                position: fixed;
                left: 0;
                top: 0;
                bottom: 0;
                width: 15px;
                ${triggerDebugCss}
            }     
            `; break;

        case 'right':  
            styleTag.innerHTML = `
            ${cssParent} .alfa-drawer-right{
            
                --width: ${this.size};
        
                display: none;
                width: var(--width);
                height: 100%;
                background: white;;
                position: absolute; 
                top: 0;
        
                right: calc(var(--width) * -1);
                transition: right ${this.transitionTime}ms ease-in-out;
            }
            ${cssParent} .alfa-drawer-right-trigger{
                position: fixed;
                right: 0;
                top: 0;
                bottom: 0;
                width: 15px;
                ${triggerDebugCss}
            }
            `; break;

        case 'top':   
            styleTag.innerHTML = `
            ${cssParent} .alfa-drawer-top{

                --height: ${this.size};
        
                height: var(--height);
                display: none;
                width: 100%;
                background: white;;
                position: absolute; 
        
                top: calc(var(--height) * -1);
                transition: top ${this.transitionTime}ms ease-in-out;
            }
            ${cssParent} .alfa-drawer-top-trigger{
                position: fixed;
                left: 0;
                top: 0;
                right: 0;
                height: 15px;
                ${triggerDebugCss}
            }
            `;  break;

        case 'bottom':
            styleTag.innerHTML = `
            ${cssParent} .alfa-drawer-bottom{

                --height: ${this.size};
        
                height: var(--height);
                display: none;
                width: 100%;
                background: white;;
                position: absolute; 
        
                bottom: calc(var(--height) * -1);
                transition: bottom ${this.transitionTime}ms ease-in-out;
            }
            ${cssParent} .alfa-drawer-bottom-trigger{
                position: fixed;
                left: 0;
                right: 0;
                bottom: 0;
                height: 15px;
                ${triggerDebugCss}
            }
            `; break;
    }

   
    styleTag.innerHTML += `

    ${cssParent} .alfa-drawer-overlay{
        display:none; 
        position: fixed; top:0; bottom:0; left:0; right:0; 
        background: black;

        opacity: 0;
        transition: opacity ${this.transitionTime}ms ease;
    }

    ${cssParent} .alfa-drawer-left-close,
    ${cssParent} .alfa-drawer-right-close,
    ${cssParent} .alfa-drawer-top-close,
    ${cssParent} .alfa-drawer-bottom-close{
        cursor: pointer;
        color: grey;
    }
    ${cssParent} .alfa-drawer-left-close{
        text-align: right;
        padding-right: .25em;
    }
    ${cssParent} .alfa-drawer-right-close{
        text-align: left;
        padding-left: .25em;
    }
    ${cssParent} .alfa-drawer-top-close{
        display: inline-block;
        padding-left: 1em;
    }
    ${cssParent} .alfa-drawer-bottom-close{
        display: inline-block;
        padding-left: 1em;
    }
    ${cssParent} .alfa-drawer-left-close:hover{color:lime}
    ${cssParent} .alfa-drawer-right-close:hover{color:lime}
    ${cssParent} .alfa-drawer-top-close:hover{color:lime}
    ${cssParent} .alfa-drawer-bottom-close:hover{color:lime}

    ${cssParent} .alfa-drawer-content{display: none} 
    `;

    styleTag.setAttribute(styleTagAttr, '');

    $dqs('head').append(styleTag);  /* calling ('body').append() will add your new styles to the bottom of the page and override any existing ones */

};    


export default AlfaDrawer;