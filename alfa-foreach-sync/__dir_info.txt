Two formats of the same code:

for NodeJS CommonJS module:
    index.js 
    package.json - optinal

for es6 module
    folder-name.js 

these files are symlinked to: 
\ALFA_MODULES_LIB - for NodeJS server side code
\WWW\\ALFA_MODULES_LIB - for front end browser code