// NEW
const fs = require('fs');
const path = require('path');

const MODULE_NAME = 'REQUIRE-es6module';

/** PRIMARY USE:
 *  for Electron
 *      convert a file with es6module (with ext .mjs)
 *      to
 *      a file with NodeJS module (with extension .js)
 * 
 *  all 'import' are converted to require()
 *  all .mjs file extensions in import are renamed to .js
 *  ASSUMING that these files are also converted ar are native NodJS modules
 * 
 * 
 * SECONDARY USE:
 *  works if a module without dependencies (internal 'import' statements) is converted
 * allows to use NodeJS require() for es6 modules
 *  NOTE:
 *  es6 modules should be in the same folder as this module (require-es6module)
 * 
 *  use:
    const requireEs6 = require('../../REQUIRE-es6module');
    const myModule = requireEs6('./my-module.mjs')
 */

    /** when doing import to require, use this keyword */
    // const REQUIRE = 'require'
    // or
    var REQUIRE = 'requireEs6'


/** 
 *  options: {
 *      write2file: true
 *      targetDir: dir where to save converted file
 *      requireStr: if write2file=true automatically set to 'require'
 *                  in other case when on the fly conversion is set, by default is 'es6require'
 *          } 
 * */    
module.exports = function (filepath, options){

    options = options || {}

    /** WARNING
     *  __dirname when symlink?
     */
    // log(MODULE_NAME, 'REQUIRE-es6module __dirname:', __dirname)

    var newFileStr=''; 

    log(filepath)
    // console.log(filepath)

    convert()

    /** SYNC function */
    function convert(){

        if(options.write2file){
            
            REQUIRE = 'require'
            var targetFile = options.targetFile
            if(options.targetDir){
                targetFile = path.join(options.targetDir, path.basename(filepath))

                /**     EXTENSION !!!
                 *      .mjs extesnsion will be interpreted by NodeJS as es6 module
                 */

                targetFile = targetFile.substr(0, targetFile.lastIndexOf('.')) + '.js'; /** change ext from .mjs */
                log('targetFile:', targetFile)
            }
            try {
                fs.unlinkSync(targetFile) /** delete old file */
            } catch(err) {/* ignore, file probably does not exist yet */}

            newFileStr = `\n/* converted from es6module on ${new Date().toISOString().slice(0,10)} */\n\n` /** extra line at the beginning is needed while debugging Electron */
        } else { /** default: return evel() */

            filepath = path.join(__dirname, filepath)
            REQUIRE = options.requireStr ? options.requireStr = 'require' : REQUIRE;
        }

        if(REQUIRE === 'requireEs6'){
            newFileStr = "const requireEs6 = require('./REQUIRE-es6module.js') \n"
        }
        
        var fileData

        try{
            fileData = fs.readFileSync(filepath, 'UTF-8');
        } catch(err){errHandler(err); return}

        // split the contents by new line
        const lines = fileData.split(/\r?\n/);

        var i = 0;
        lines.forEach((line) => {
            processLine(line, ++i)
        });


//         console.log(`\x1b[35m    converted, newFileStr:
// ${newFileStr}
// \x1b[0m`)

        if(options.write2file){
            fs.writeFile(targetFile, newFileStr, (err) => { 
                if (err) errHandler(err); 
            }) 
        } else {
            return  eval(newFileStr); // -------------------------------------------- >
        }
    }

    function processLine(line, i){

        var oldLine = line.trim()
        if(oldLine==='') return;

        log(i || '', oldLine)

        if(oldLine.startsWith('import') ){
            
            var newLine = import2require(oldLine) 
            if(REQUIRE==='require')
                newLine = newLine.replace(/.mjs/g, '.js') /** NodeJS will interpret .mjs as es6 module, 
                                                            it has to be changed if require() is used 
                                                            assuming that the module is already converted */
            addLine(newLine, oldLine)
        }
      
        else if(oldLine.startsWith('export') ){

            var newLine = replace_export(oldLine)
            addLine(newLine, oldLine)
        }

        else {
            newFileStr += line + '\n'
        }
    }
    function addLine(newLine, oldLine){

        newFileStr += newLine + '\n'

        log(`\x1b[34m${oldLine}`)
        log(`\x1b[32m${newLine} \x1b[0m `)
    }

  
    function replace_export(line){

        /** 1. find keyword 'export default' and it's position */

        var newLine = replaceKeyword('export default', line);

        /** 1a. if previous not found, find keyword 'export' and it's position */
        if(!newLine) {
            newLine = replaceKeyword('export', line);
        }
    
        //OLD when all file was scanned: if(!newLine){errHandler({message: `Module code not converted. 'export' keyword not found!`, filepath});}
    
        
        return  newLine; // ------------------------------------------ >
    
        /** returns newLine with replaced keyword or null */
        function replaceKeyword(keyword2replace, line){
            

            if(!line.startsWith(keyword2replace)) {return null}

            // var P = checkKeywordPosition(line, keyword2replace, 0);
            // if (P===-1) return null;

            var P=0;    /** after triming, it should be 0 */
    
            return replaceBetween.call(line, {
                start: P, 
                end:   P + keyword2replace.length+1, 
                what: 'module.exports = '});
        }
    
        // function errHandler(err){
        //     if(typeof options.onError==='function') options.onError(err);
        //     else console.error(`[${MODULE_NAME}] ERR: ${filepath}`, err)
        // }

    }
    function errHandler(err){
        if(typeof options.onError==='function') options.onError(err);
        else console.error(`[${MODULE_NAME}] ERR: ${filepath}`, err)
    }

    /** replace text between two indices
     *  use:
        var s = replaceBetween.call(myStr, {
                start: P, 
                end:   P + str2replace.length + 1, 
                what: 'module.exports = '});
    */
    function replaceBetween(args) {
        return this.substring(0, args.start) + args.what + this.substring(args.end);
    }; 

}//end of module


var log = function(){
    return
    console.log.apply(null, arguments);
}



/*  Import To Require Syntax ***************************************************************************************
    VSCode extension
    https://github.com/TLevesque/import-2-require
*/

// const vscode = require("vscode");

// 3.
const getAllIndexes = (arr, func) => {
  let indexes = [];
  arr.forEach((line, i) => {
    if (func(line)) indexes.push(i);
  });
  return indexes;
};

// 2.
const splitString = string => {
  const trimString = string.trim();
  const lineArr = trimString.split("\n").map(line => line.trim());
  const importIndex = getAllIndexes(lineArr, line => line === "import {");
  const fromIndex = getAllIndexes(
    lineArr,
    line =>
      line.match(/} from/) && !line.match(/import {/) && !line.match(/, {/)
  );
  if (importIndex.length > 0 && fromIndex.length > 0) {
    const indexList = importIndex
      .map((importInd, i) => [importInd, fromIndex[i]])
      .map(arr => {
        const indexes = [];
        let i = +arr[0];
        while (i <= +arr[1]) {
          indexes.push(i);
          i++;
        }
        return indexes;
      })
      .reduce((prev, curr) => prev.concat(curr));
    const arrays = importIndex
      .map((index, i) => lineArr.slice(index, fromIndex[i] + 1))
      .map(arr => arr.join(""));
    lineArr.forEach((line, i) => {
      if (!indexList.includes(i)) arrays.push(line);
    });
    return arrays;
  } else {
    return lineArr.filter(line => line.length !== 0);
  }
};

// 4a
const extractPath = string => { //5
  const matchedText = string.match(/(\"|\')(.+?)(\"|\')/i);
  return matchedText && matchedText[0].trim();
};
// 4b
const extractName = string => {
  const matchedText = string.match(/import(.*?)from/);
  return matchedText && matchedText[1].trim();
};

/** replace 'import' with 'require' *******************************************  */
// 4
const createRequireString = string => {

  const path = extractPath(string);
  const name = extractName(string);

//   const REQUIRE = 'requireEs6' declared at the top

  if (!name && path) return `${REQUIRE}(${path});`;

  if (
    name &&
    name.match(/\{/i) &&
    name.match(/\}/i) &&
    path &&
    !path.match(/\.\//i)
  ) {
    const nameSring = string.match(/{(.+?)}/i)[1].trim();
    const extraName =
      string
        .match(/import(.+?){/i)[1]
        .trim()
        .replace(",", "") ||
      string
        .match(/}(.+?)from/i)[1]
        .trim()
        .replace(",", "") ||
      null;
    if (nameSring.includes(",")) {
      const names = nameSring.split(",").map(name => name.trim());
      let returnedString = extraName
        ? `const ${extraName} = ${REQUIRE}(${path});\n`
        : "";
      names.forEach((name, i) => {
        if (name.includes(" as ")) {
          const [originalName, newName] = name
            .split(" as ")
            .map(name => name.trim());
          if (i === names.length - 1) {
            returnedString = `${returnedString}const ${newName} = ${REQUIRE}(${path}).${originalName};`;
          } else {
            returnedString = `${returnedString}const ${newName} = ${REQUIRE}(${path}).${originalName};\n`;
          }
        } else {
          if (i === names.length - 1) {
            returnedString = `${returnedString}const ${name} = ${REQUIRE}(${path}).${name};`;
          } else {
            returnedString = `${returnedString}const ${name} = ${REQUIRE}(${path}).${name};\n`;
          }
        }
      });
      return returnedString;
    } else {
      if (nameSring.includes(" as ")) {
        const [originalName, newName] = nameSring
          .split(" as ")
          .map(name => name.trim());
        return extraName
          ? `const ${extraName} = ${REQUIRE}(${path});\nconst ${newName} = ${REQUIRE}(${path}).${originalName};`
          : `const ${newName} = ${REQUIRE}(${path}).${originalName};`;
      } else {
        return extraName
          ? `const ${extraName} = ${REQUIRE}(${path});\nconst ${nameSring} = ${REQUIRE}(${path}).${nameSring};`
          : `const ${nameSring} = ${REQUIRE}(${path}).${nameSring};`;
      }
    }
  }

  if (
    name &&
    name.match(/\{/i) &&
    name.match(/\}/i) &&
    path &&
    path.match(/\.\//i)
  ) {
    const nameSring = string.match(/{(.+?)}/i)[1].trim();

    if (nameSring && nameSring.includes(",")) {
      const names = nameSring
        .split(",")
        .map(name => name.trim())
        .filter(name => name.length !== 0);
      let returnedString = "";
      names.forEach((name, i) => {
        if (i === names.length - 1) {
          returnedString = `${returnedString}const ${name} = ${REQUIRE}(${path}).${name};`;
        } else {
          returnedString = `${returnedString}const ${name} = ${REQUIRE}(${path}).${name};\n`;
        }
      });
      return returnedString;
    } else {
      return `const ${nameSring} = ${REQUIRE}(${path}).${nameSring};`;
    }
  }

  if (
    name &&
    !name.match(/\{/i) &&
    !name.match(/\}/i) &&
    path &&
    path.match(/\.\//i)
  ) {
    if (name.includes(" * as ")) {
      const modifiedName = name.replace(" * as ", "").trim();
      return `const ${modifiedName} = ${REQUIRE}(${path});`;
    } else {
      return `const ${name} = ${REQUIRE}(${path});`;
    }
  }

  return `const ${name} = ${REQUIRE}(${path});`;
};

/** replace 'import' with 'require' *******************************************  */
const createRequireString_OLD = string => {

    const path = extractPath(string);
    const name = extractName(string);
  
    const REQUIRE = 'require'
  
    if (!name && path) return `require(${path});`;
  
    if (
      name &&
      name.match(/\{/i) &&
      name.match(/\}/i) &&
      path &&
      !path.match(/\.\//i)
    ) {
      const nameSring = string.match(/{(.+?)}/i)[1].trim();
      const extraName =
        string
          .match(/import(.+?){/i)[1]
          .trim()
          .replace(",", "") ||
        string
          .match(/}(.+?)from/i)[1]
          .trim()
          .replace(",", "") ||
        null;
      if (nameSring.includes(",")) {
        const names = nameSring.split(",").map(name => name.trim());
        let returnedString = extraName
          ? `const ${extraName} = require(${path});\n`
          : "";
        names.forEach((name, i) => {
          if (name.includes(" as ")) {
            const [originalName, newName] = name
              .split(" as ")
              .map(name => name.trim());
            if (i === names.length - 1) {
              returnedString = `${returnedString}const ${newName} = require(${path}).${originalName};`;
            } else {
              returnedString = `${returnedString}const ${newName} = require(${path}).${originalName};\n`;
            }
          } else {
            if (i === names.length - 1) {
              returnedString = `${returnedString}const ${name} = require(${path}).${name};`;
            } else {
              returnedString = `${returnedString}const ${name} = require(${path}).${name};\n`;
            }
          }
        });
        return returnedString;
      } else {
        if (nameSring.includes(" as ")) {
          const [originalName, newName] = nameSring
            .split(" as ")
            .map(name => name.trim());
          return extraName
            ? `const ${extraName} = require(${path});\nconst ${newName} = require(${path}).${originalName};`
            : `const ${newName} = require(${path}).${originalName};`;
        } else {
          return extraName
            ? `const ${extraName} = require(${path});\nconst ${nameSring} = require(${path}).${nameSring};`
            : `const ${nameSring} = require(${path}).${nameSring};`;
        }
      }
    }
  
    if (
      name &&
      name.match(/\{/i) &&
      name.match(/\}/i) &&
      path &&
      path.match(/\.\//i)
    ) {
      const nameSring = string.match(/{(.+?)}/i)[1].trim();
  
      if (nameSring && nameSring.includes(",")) {
        const names = nameSring
          .split(",")
          .map(name => name.trim())
          .filter(name => name.length !== 0);
        let returnedString = "";
        names.forEach((name, i) => {
          if (i === names.length - 1) {
            returnedString = `${returnedString}const ${name} = require(${path}).${name};`;
          } else {
            returnedString = `${returnedString}const ${name} = require(${path}).${name};\n`;
          }
        });
        return returnedString;
      } else {
        return `const ${nameSring} = require(${path}).${nameSring};`;
      }
    }
  
    if (
      name &&
      !name.match(/\{/i) &&
      !name.match(/\}/i) &&
      path &&
      path.match(/\.\//i)
    ) {
      if (name.includes(" * as ")) {
        const modifiedName = name.replace(" * as ", "").trim();
        return `const ${modifiedName} = require(${path});`;
      } else {
        return `const ${name} = require(${path});`;
      }
    }
  
    return `const ${name} = require(${path});`;
  };

/** main entry point for import t0 require 
 * 1.
*/
const import2require = string => {

  const arrayedString = splitString(string);
  if (arrayedString.length === 1) {
    return createRequireString(arrayedString[0].trim());
  } else {
    const convertedStrings = arrayedString
      .filter(line => line.length !== 0)
      .map(line => createRequireString(line.trim()));
    let returnedString = "";
    convertedStrings.forEach((line, i) => {
      if (i === arrayedString.length - 1) {
        returnedString = `${returnedString}${line}`;
      } else {
        returnedString = `${returnedString}${line}\n`;
      }
    });
    return returnedString;
  }
};



// const insertText = val => {
//   const editor = vscode.window.activeTextEditor;

//   if (!editor) {
//     vscode.window.showErrorMessage(
//       "Can't insert log because no document is open"
//     );
//     return;
//   }

//   const selection = editor.selection;

//   const range = new vscode.Range(selection.start, selection.end);

//   const returnedString = import2require(val);

//   editor.edit(editBuilder => {
//     editBuilder.replace(range, returnedString);
//   });
// };

// function getAllExports(document, documentText) {
//   let exportStrings = [];

//   const exportRegex = /^export const /gm;
//   let match;
//   while ((match = exportRegex.exec(documentText))) {
//     let matchRange = new vscode.Range(
//       document.positionAt(match.index),
//       document.positionAt(match.index + match[0].length)
//     );
//     if (!matchRange.isEmpty) exportStrings.push(matchRange);
//   }
//   return exportStrings;
// }

// function getAllExportDefaults(document, documentText) {
//   let exportDefaultStrings = [];

//   const exportDefaultRegex = /^export default |^export /gm;
//   let match;
//   while ((match = exportDefaultRegex.exec(documentText))) {
//     let matchRange = new vscode.Range(
//       document.positionAt(match.index),
//       document.positionAt(match.index + match[0].length)
//     );
//     if (!matchRange.isEmpty) exportDefaultStrings.push(matchRange);
//   }
//   return exportDefaultStrings;
// }

// /** replace 'exports'   ************************************************************* */
// function replaceAllFoundExports(exportStrings, exportDefaultStrings) {
//   const editor = vscode.window.activeTextEditor;

//   if (exportStrings.length > 0) {
//     const exportStringList = Array.of(exportStrings)[0];
//     let counter = 0;
//     const convertString = (exportStringList, counter) => {
//       const exportReplacement = "exports.";
//       editor
//         .edit(editBuilder => {
//           const convertedExportString = Object.entries(
//             exportStringList[counter]
//           );
//           const start = convertedExportString[0][1];
//           const end = convertedExportString[1][1];
//           const range = new vscode.Range(start, end);
//           editBuilder.replace(range, exportReplacement);
//         })
//         .then(() => {
//           counter++;
//           if (counter < exportStringList.length) {
//             convertString(exportStringList, counter);
//           }
//         });
//     };
//     if (counter < exportStringList.length) {
//       convertString(exportStringList, counter);
//     }
//   }

//   if (exportDefaultStrings.length > 0) {
//     const exportDefaultStringList = Array.of(exportDefaultStrings)[0];
//     let counterDefault = 0;
//     const exportDefaultReplacement = "module.exports = ";
//     const convertString = (exportDefaultStringList, counterDefault) => {
//       editor
//         .edit(editBuilder => {
//           const convertedExportString = Object.entries(
//             exportDefaultStringList[counterDefault]
//           );
//           const start = convertedExportString[0][1];
//           const end = convertedExportString[1][1];
//           const range = new vscode.Range(start, end);
//           editBuilder.replace(range, exportDefaultReplacement);
//         })
//         .then(() => {
//           counterDefault++;
//           if (counterDefault < exportDefaultStringList.length) {
//             convertString(exportDefaultStringList, counterDefault);
//           }
//         });
//     };
//     if (counterDefault < exportDefaultStringList.length) {
//       convertString(exportDefaultStringList, counterDefault);
//     }
//   }
// }

// /** begin of extension */

// function activate(context) {
//   const replaceAllImports = vscode.commands.registerCommand(
//     "extension.replaceAllImports",
//     () => {
//       const editor = vscode.window.activeTextEditor;
//       if (!editor) {
//         return;
//       }

//       const selection = editor.selection;
//       const text = editor.document.getText(selection);

//       insertText(text);
//     }
//   );
//   context.subscriptions.push(replaceAllImports);

//   const replaceAllExports = vscode.commands.registerCommand(
//     "extension.replaceAllExports",
//     () => {
//       const editor = vscode.window.activeTextEditor;
//       if (!editor) {
//         return;
//       }

//       const document = editor.document;
//       const documentText = editor.document.getText();

//       const exportStrings =        getAllExports(document, documentText);
//       const exportDefaultStrings = getAllExportDefaults(document, documentText);

//       replaceAllFoundExports(exportStrings, exportDefaultStrings);
//     }
//   );
//   context.subscriptions.push(replaceAllExports);
// }
// exports.activate = activate;

// function deactivate() {}
// exports.deactivate = deactivate;
