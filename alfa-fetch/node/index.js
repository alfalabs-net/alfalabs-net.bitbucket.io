
const requireEs6 = require('../../REQUIRE-es6module'); /** usage of es6 module from ALFA_es6_and_node_modules */

var MODULE_NAME = 'alfa-fetch';

module.exports = function(req, res){
    // console.log(`[${MODULE_NAME}] index.js`)
    var RESULT = {
        MODULE_NAME,
        info: 'server side index.js module',
    }

    var coreCallbacks = require('../../_node-server/core-callbacks')(req, res , {MODULE_NAME});


    switch (req.params.cmd){
        case 'sync-api':   coreCallbacks.sendResutl(RESULT); break;
        case 'async-api':  asyncApi(); break;
        case 'info': RESULT.info = 'result from NodeJS server';    coreCallbacks.sendResutl(RESULT); break;
        case 'err':  RESULT.error = {message:'error from server'}; coreCallbacks.sendResutl(RESULT); break;
        default: defaultResponse();
    }

    function asyncApi(){
        var value = req.params.value;
        value = value * 1000;
        setTimeout(function(){
            RESULT.delay = value;
            coreCallbacks.sendResutl(RESULT);
        }, value)
        
    }
  

    function defaultResponse(){
        RESULT.error = {message:'cmd unknown.', params: req.params}
        coreCallbacks.sendResutl(RESULT)
    }
 
   
}