﻿'use strict';
const express = require('express');

const app = express();
app.disable('x-powered-by')

const path = require('path');
const bodyParser = require('body-parser');

/**
 *  we are in __dirname: 
 *      C:\CODE\ALFA_es6_and_node_modules\_node-server
 *  we want public www folder in: 
 *      C:\CODE\ALFA_es6_and_node_modules
 *  remove last dir from __dirname
 */
var www = __dirname.split('\\')
    www.pop();
    www = www.join('\\')
app.locals.www = www;
// console.log('www', www)

app.use(express.static(www));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// var favicon = require('serve-favicon');
// app.use(favicon(path.join(__dirname, 'WEB','favicon.ico')));

/* routes */

app.get('/', function (req, res) {
    res.sendFile('/index.html');
});

app.use('/node', require('./apps-routes'));

// app.get('/node/alfa-foreach-sync', function(req, res){
//     // res.end('test response from alfa-core');
//     require('../alfa-foreach-sync/node/index.js')(req, res);
// })

// app.get('/test/:type', function (req, res) {
//     const test1 = require('./test1');
//     test1(req, res);
// });

// catch 404
app.use(function (req, res) {
    var err = {status:404, message:`404 Page Not Found: '${req.originalUrl}' ${req.method}`};
    // var result={err, errors:[], data:null}; - BAD
    var result = {error:err, errors:[], data:null};
    res.send(result);
});

module.exports = app;
