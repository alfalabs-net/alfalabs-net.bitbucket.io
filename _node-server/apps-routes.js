const express = require('express');
const routes = express.Router();

/** only modules having NodeJS code in /node dir 
 * 
 *  this file called from:
 *  app.use('/node', require('./apps-routes'));
 * 
 *  example:
 *  http://localhost:3001/node/alfa-auth-user-ui
*/

routes.get('/aaa-template/:cmd?', function(req, res){
    require('../aaa-template/node/index.js')(req, res);
})
routes.get('/REQUIRE-es6module-test/:cmd?', function(req, res){
    require('../REQUIRE-es6module-test/node/index.js')(req, res);
})
routes.get('/alfa-foreach-sync', function(req, res){
    require('../alfa-foreach-sync/node/index.js')(req, res);
})
routes.get('/alfa-fetch/:cmd/:value?', function(req, res){
    require('../alfa-fetch/node/index.js')(req, res);
})



// TODO:
// routes.use('/alfa-auth-user', require('../alfa-auth-user-ui/node/app-routes'));


// routes.post('/alfa-auth-user-ui', function(req, res){
//     require('../alfa-auth-user-ui/node/alfa-auth.js')(req, res);
// })
// routes.get('/alfa-auth-user-ui/:cmd/:value?', function(req, res){ 
//     require('../alfa-auth-user-ui/node/alfa-auth.js')(req, res); 
// })
// new
routes.use('/alfa-auth-user-ui', require('../alfa-auth-user-ui/node/app-routes'));


module.exports = routes;