/** es6 module - example with named object */

var myObj = {
    name: 'example-with-named.mjs',
    g: 'gamma',
    d: 'delta',
    log: console.log,
    warn: console.warn
}
export {myObj}

/*  named object

    STATIC import

    import {myObj} from './example-with-named.mjs'


    DYNAMIC import()
    NOTE: this is NOT a function, just syntax with () looking as JS function

    import('./example-with-named.mjs')
    .then(function({myObj}){        <== NOTE: imported object comes as it was exported with curlies
        myObj.log('hidere 2')
    })

*/