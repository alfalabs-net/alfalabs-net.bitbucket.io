/*  NOT WORKING !
    helper for alfa-console-log.mjs
    makes log functions based on instance of AlfaConsoleLog created in module and passes them to module
*/
var log, logInfo, logWarn, logErr;

function alfaConsoleLog_init(alfaConsoleLog){

    log =     alfaConsoleLog.log.bind(alfaConsoleLog)
    logInfo = alfaConsoleLog.logInfo.bind(alfaConsoleLog)
    logWarn = alfaConsoleLog.logWarn.bind(alfaConsoleLog)
    logErr =  alfaConsoleLog.logErr.bind(alfaConsoleLog)

}


export {alfaConsoleLog_init, log, logInfo, logWarn, logErr}