/*! alfa-databinding    - for non Polymer elements     Copyright (c) alfalabs.net license MIT */
/*  usage:
 * 
 *    to bind html elemts on a page
 * 
 *      import {AlfaDatabinding} from '../alfa-databinding.mjs'
        new AlfaDatabinding(document)

      or to bind elements and use methods

        import {AlfaDatabinding} from '../alfa-databinding.mjs'
        var alfaDatabinding = new AlfaDatabinding(document)
 */

import {setPropertyByPath, getPropertyByPath} from './alfa-setproperty-bypath.mjs'

import AlfaConsoleLog from './alfa-console-log.mjs'; 
var alfaConsoleLog = new AlfaConsoleLog('alfa-databinding', {logLevel: 1})
var log =     alfaConsoleLog.log.bind(alfaConsoleLog)
var logInfo = alfaConsoleLog.logInfo.bind(alfaConsoleLog)
var logWarn = alfaConsoleLog.logWarn.bind(alfaConsoleLog)
var logErr =  alfaConsoleLog.logErr.bind(alfaConsoleLog)

const $dce = document.createElement.bind(document)

/**
 * 
 * @param pageDoc - DOM object to scan for html components with bound_paths 
 *                  @default = document object
 *                  elements inside shadow DOM will not be found!
 *                  in web-componet, an instance can be invoked with this.shadowRoot as pageDoc
 * @param ds_root - Data Source root, 
 *                  @default = window object
 */
function AlfaDatabinding(pageDoc, ds_root){

    // assuming default - if(!pageDoc){console.error('new AlfaDatabinding(pageDoc) pageDoc is falsy!'); return}
    pageDoc = pageDoc || document
    this.ds_root = ds_root || window /* Data Source root */

    this.$qsa = pageDoc.querySelectorAll.bind(pageDoc)

    this.initialize()
    
    this.bindings_byPath = {}   /** object contains Arrays of elems subscribing to this pathString */
    
    this.scanPage()     /** scan page and find all controls with databinding */

}

AlfaDatabinding.prototype.initialize = function(){

    /** any variable can be attached to this.ds_root */

    /** TODO to control scope of variables */
    // this.ds_root.dsPage =  {}
    // this.ds_root.dsApp =   {}    
    // this.ds_root.dsGlob = {}   

    this.ds_root.dsConst = {
        TRUE: true,
        FALSE: false,
        ZERO: 0,
        ONE: 1,
        NULL: null,
        EMPTY: ''
    }    
}

/** scan DOM page and find all controls with databinding */
AlfaDatabinding.prototype.scanPage = function(){

    this.$qsa('*').forEach(function(elem){
        this.createBinding(elem)
    }.bind(this))

    // log('bindings:', this.bindings)
    log('bindings_byPath:', this.bindings_byPath)

}
/** find databinding attributes and store bindings */
AlfaDatabinding.prototype.createBinding = function(elem){

    elem.alfa_bindings = []

    var attrs = Array.prototype.slice.call(elem.attributes);

    attrs.forEach(function(attr){
        
        if(attr.name.startsWith('bound_path')){

            var pathName = attr.name,       /* ie: 'bound_path_out'    */
                pathString = attr.value     /* ie: 'dsPage.in1'        */

            /** needed when element onChange fires */
            elem.alfa_bindings.push({
                pathName,
                pathString 
            })

            /** needed for subscriptions     */
            this.bindings_byPath[pathString] = this.bindings_byPath[pathString] || []
            this.bindings_byPath[pathString].push({elem, pathName})

            /** 2. make event */
            if(elem.tagName==='INPUT' || elem.tagName==='SELECT'){
                elem.addEventListener('change', this.onChange.bind(this))
            }

            /** 3. for SELECT, start with blank choice */
            if(elem.tagName==='SELECT' && elem.hasAttribute('start-blank')){
                elem.selectedIndex = -1
            }

            /** 4. for web components */
            // if(elem.tagName.includes('-')){  } - no need for event

        }
    }.bind(this))

}

/** web-components are hidden in shadow DOM, can not be found by this.scanPage()
 *  they have to be added in ready() event of the component
 */
AlfaDatabinding.prototype.addWebComponent = function(elem){
    
    this.createBinding(elem) /** get attributes startsWith('bound_path') and create elem.alfa_bindings Array */
  
}

AlfaDatabinding.prototype.onChange = function(ev){

    var elem = ev.target
    // log('on change:', elem) // for input, fires when chaged input looses focus!

    // if(!this.validateAndFormat(ev.target)) return     // --- >

    /** element on change sends value OUT either to bound_path or bound_path_out     */
    var pathOut, bound_path_out;
    /* find pathNames for sending data out      */
    elem.alfa_bindings.forEach(function(binding){
        /* only bound_path and bound_path_out can send data out on change
         * if elem has bound_path_out, change will be sent there         */
        if(binding.pathName === 'bound_path_out'){ // bound_path_out has priority over bound_path
            pathOut = binding.pathString;
            bound_path_out = true;
        } else {
            if(!pathOut && binding.pathName === 'bound_path'){pathOut = binding.pathString;}
        }
        /* other pathNames are not sending data out     */
    })

       // elements based on Array should have bound_path_out 
       if(elem.tagName==='SELECT' || elem.tagName==='TABLE'){
            if(!bound_path_out){logWarn.call(this, elem.tagName, 'does not have bound_path_out and on-change is ignored!', elem);
            return;
        }
    }

    var value = this.elem_getValue(elem)

    if(pathOut){ this.set(pathOut, value, elem, logErr); }

}

/**
 * 
 * @param path 
 * @param value 
 * @param settingElem - @required when an element onChange does the setting
 * @param errCallback 
 * @param opts 
 */
AlfaDatabinding.prototype.set = function(path, value, settingElem, errCallback, opts){

    if(typeof path !=='string'){   logErr('set(path, value) path not string!', {path, value}); return}
    if(path.startsWith('dsConst')){logErr('set() can not change CONSTANT:', {path, value}); return}
    if(typeof settingElem==='undefined'){logErr('function set(path, value, settingElem) has 3 required args, settingElem can be null'); return}
    
    /** @param 'this.ds_root' is a root object of dataSources ie dsPage, dsApp */
    setPropertyByPath(this.ds_root, path, value, errCallback, opts);
    
    this.notifySubscribers(path, value, settingElem);
}
AlfaDatabinding.prototype.get = function(path, errCallback){
    return getPropertyByPath(this.ds_root, path, null, errCallback)
}

/** assign a value from other data path */
AlfaDatabinding.prototype.set_fromPath = function(target_path, source_path, errCallback, opts){
    var sourceValue = this.get(source_path, errCallback)
    this.set(target_path, sourceValue, null, errCallback, opts)
}


/**
 * @param path - pathString ie: 'dsPage.in1'  
 * @param value 
 * @param settingElem - needed to prevent setting a value on the onchange originating element
 */
AlfaDatabinding.prototype.notifySubscribers = function(path, value, settingElem){

    // undefined value should be called earlier, but just in case:
    if(typeof value==='undefined'){ logWarn('notifySubscribers() aborted.', {path, value}); return}
    if(typeof settingElem==='undefined'){ logErr('notifySubscribers() settingElem is undefined!', {path, value}); return}
    
    // var elem, pathName
   
    if(Array.isArray(this.bindings_byPath[path])){   /** check if exact path does have subscribers */
        
        this.bindings_byPath[path].forEach(function(item){
            var elem = item.elem
            var pathName = item.pathName
            if(elem !==settingElem){
    
                this.elem_setValue(elem, value, pathName)
                log('notifySubscribers(Par)', pathName, elem)
            }
        }.bind(this))

    } else {  /** check if path children do have subscribers */

        for(var pathString in this.bindings_byPath){

            if(pathString.startsWith(path)){
                if(isPartialPath(pathString, path.length)){ return } // --- >

                this.bindings_byPath[pathString].forEach(function(item){
                    var elem = item.elem
                    var pathName = item.pathName
                    if(elem !==settingElem){

                        /*  value passed into notifySubscribers() is the value of child,
                            we are notifying parent,
                            find current value of parent                        */
                        value = this.get(pathString, logErr);
            
                        this.elem_setValue(elem, value, pathName)
                        log('notifySubscribers(Kid)', pathName, elem)
                    }
                }.bind(this))
            }
        }

    }
    /////////////////
    // if(elem !==settingElem){
    
    //     this.elem_setValue(elem, value, pathName)
    // }
    
    /* check if path is a partial path of object */
    function isPartialPath(pathToCheck, dotPosition){
        if(pathToCheck.charAt(dotPosition)==='.'){
            return false
        } else {
            return true;  /*  pathToCheck coicidentially starts with similar chars as other path */
        }
    }
   
}

/** for specific html elements  ***************************************************************** */

AlfaDatabinding.prototype.elem_getValue = function(elem){
    if(elem.tagName==='INPUT' && elem.getAttribute('type')==='checkbox'){
        return elem.checked
    } else
        return elem.value
}
AlfaDatabinding.prototype.elem_setValue = function(elem, value, pathName){

    /** web-component */
    if(elem.tagName.includes('-') && elem.databinding_setValue){
        elem.databinding_setValue(value, pathName)    /** web-component has to have it's own method to handle the new value */
        return  // --- >
    }

    if(pathName==='bound_path'){

        if(elem.tagName==='INPUT' && elem.getAttribute('type')==='checkbox'){
            elem.checked = value
        } else if(elem.tagName==='INPUT'){  /** input has value attribute */
            elem.value = value
        } else if(elem.tagName==='SELECT' || elem.tagName==='DATALIST'){
            this.make_html_options(elem, value)
        } else if(elem.tagName==='TABLE'){
            this.make_html_table(elem, value)
        } else if(elem.tagName==='PRE' && elem.hasAttribute('to-json')){
            elem.innerText =  JSON.stringify(value, null, 4)
        // } else if(elem.tagName.includes('-')){ /** web-component */
        //     elem.databinding_setValue(value, pathName)    /** web-component has to have it's own method to handle the new value */
        } else {            /** span has not */
            elem.innerText = value
        }
    } else {
        this.elem_action(elem, value, pathName)
    }
}
AlfaDatabinding.prototype.elem_action = function(elem, value, pathName){
    
    switch(pathName){
        case 'bound_path_disabled':
            if(value) {elem.setAttribute('disabled', '')} else {elem.removeAttribute('disabled')}; break
        case 'bound_path_hidden':
            if(value) {
                elem.style.display = 'none'
            } else {
                /** unhiding needs to know the display before hide */
                var visibleDisplay = elem.getAttribute('visible-display') || 'unset'
                elem.style.display = visibleDisplay
            }; break
        case 'bound_path_select_item':      /** selecting by value in <SELECT><OPTION> */
            if(elem.tagName==='SELECT'){
                var options =  elem.querySelectorAll('option')
                options.forEach(function(item, i){
                    if(item.value===value){elem.selectedIndex = i}
                })
            }; break
        case 'bound_path_select_by_index':
            if(elem.tagName==='TABLE'){
                var tr = elem.querySelector(`[row-index="${value}"]`)
                if(tr){
                    this.onTblRowClick.call({alfaDatabinding: this, tableElem: elem}, {target: tr})
                }
            }; break
        case 'bound_path_clear_selected':
            if(elem.tagName==='TABLE'){
                var trs = elem.querySelectorAll('tr')
                trs.forEach(function(tr){
                    tr.removeAttribute('selected')
                })
            }; 
            if(elem.tagName==='SELECT'){
                elem.selectedIndex = -1
            }
            break
    }
}

/** for <select><option> and <datalist><option>
 * @param parentElem - <select> html element
 * @param list - Array [{'item name': value}, ...]
 */
AlfaDatabinding.prototype.make_html_options = function(parentElem, list){
   
    clearNode(parentElem)  // parentElem.innerHTML = ''

    if(parentElem.hasAttribute('first-empty')){list.unshift({'': null})} /** good to have for unselecting */

    list.forEach(function(item){
        var itemName, itemValue

        if(typeof item==='object'){ /** expected format: {'item name': value} */
            for(itemName in item){itemValue = item[itemName]}
            if(parentElem.hasAttribute('text-value')){itemName = `${itemName}: ${itemValue}` }
            if(parentElem.hasAttribute('value-text')){itemName = `${itemValue}: ${itemName}` }
            if(parentElem.hasAttribute('value-only')){itemName = itemValue }
        } else {                    /** expected string */
            itemName = item
            itemValue = item
        }

        var option = $dce('option')
        option.text = itemName
        option.value = itemValue
        parentElem.append(option)
    })

    if(parentElem.hasAttribute('start-blank')){parentElem.selectedIndex = -1}
}
/** for <table>
 * @param tableElem - <table>
 * @param list - Array [{colName: colVelue, colName2: colValue2, ... }]
 */
AlfaDatabinding.prototype.make_html_table = function(tableElem, list){
   
    clearNode(tableElem) // tableElem.innerHTML = ''

    var tbody = $dce('tbody')
    list.forEach(function(row, i){
        var colName
        var tr = $dce('tr')
        tr.setAttribute('row-index', i)
        for(colName in row){
            var td = $dce('td')
            td.innerHTML = row[colName]
            tr.append(td)
        }
        tr.addEventListener('click', this.onTblRowClick.bind({alfaDatabinding: this, tableElem}))
        tbody.append(tr)
    }.bind(this))
    tableElem.append(tbody)
}
AlfaDatabinding.prototype.onTblRowClick = function(ev){

    /** bind({alfaDatabinding: this, tableElem}) */

    if(this.tableElem.hasAttribute('disabled')) return;

    var tr = ev.target.closest('tr');

    var index = tr.getAttribute('row-index')
    var pathIn = this.tableElem.getAttribute('bound_path')
    var pathOut = this.tableElem.getAttribute('bound_path_out')

    if(pathOut){
        var sourceArr = this.alfaDatabinding.get(pathIn, logErr)

        this.alfaDatabinding.set(pathOut, sourceArr[index], this.tableElem)
    } else {
        logErr(`onTblRowClick(${index}) bound_path_out is falsy!`)
    }

    /** making slected attr for use by hostpage CSS */
    var trs = this.tableElem.querySelectorAll('tr')
    trs.forEach(function(tr){
        tr.removeAttribute('selected')
    })
    tr.setAttribute('selected', '')
}


export {AlfaDatabinding};


function clearNode(myNode){
    while (myNode.firstChild) {
        myNode.removeChild(myNode.lastChild);
    }
}
