
/** alternative 1    */
import AlfaConsoleLog from '../alfa-console-log.mjs'; 
var alfaConsoleLog = new AlfaConsoleLog('module-two')

var log =     alfaConsoleLog.log.bind(alfaConsoleLog)
var logInfo = alfaConsoleLog.logInfo.bind(alfaConsoleLog)
var logWarn = alfaConsoleLog.logWarn.bind(alfaConsoleLog)
var logErr =  alfaConsoleLog.logErr.bind(alfaConsoleLog)

/** alternative 2   NOT WORKING!     */
// import AlfaConsoleLog from '../alfa-console-log.mjs'; 
// import {alfaConsoleLog_init, log, logInfo, logWarn, logErr} from '../alfa-console-log-make.mjs';
// var alfaConsoleLog = new AlfaConsoleLog('module-two.mjs')     
// alfaConsoleLog_init(alfaConsoleLog)


// NOT working! /////////////
// import {alfaConsoleLog, log, logInfo, logWarn, logErr} from '../alfa-console-log-2.mjs';

function ModuleTwo(){

    this.id = 'M2'
    alfaConsoleLog.setCfg({
        // moduleName: 'module-two.mjs',
        id: this.id,
        color: 'brown'
    })
    // log.call(this, 'constructor for ModuleTwo') - no need to call(this)
    log('constructor for ModuleTwo')
    logInfo('ModuleTwo info')
    setTimeout(function(){log.call(this, 'from ModuleTwo again')}, 2000)
}
// ModuleTwo.prototype = function(){}


export default ModuleTwo