/** json prettify, produce html code to be inserted into <pre> element */

const $dqs = document.querySelector.bind(document); 
const $dce = document.createElement.bind(document); 

const ATTR = 'alfa-print-object'

/** constructor  */
function AlfaJsonPretty(){
 
    this.insertStyleTag()

}
/** main function: */
AlfaJsonPretty.prototype.getHtml = function(obj, replacer, spacer, preserveJson){
    
    replacer = replacer || null
    spacer = spacer || 4
    
    var jsonStr = this.safeStringify(obj, replacer, spacer)

    if(!preserveJson) {
        jsonStr = jsonStr.replace(/"(\w+)"\s*:/g, '$1:') /* remove quotes around keys */
        jsonStr = jsonStr.replace(/^\s*\{|}\s*$/g, '')   /* remove enclosing curly brackets */
        jsonStr = jsonStr.replace(/^.*[\n\r]/g, '') /* remove first emty line */
    }

    return this.syntaxHighlight(jsonStr)
}

AlfaJsonPretty.prototype.syntaxHighlight = function(json) {
    if(!json) return;
    
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return `<span ${ATTR} class="${cls}">${match}</span>`;
        // return '<span class="' + cls + '">' + match + '</span>';
    });
};


AlfaJsonPretty.prototype.safeStringify = function(obj, replacer, spacer) {
    // https://github.com/davidmarkclements/fast-safe-stringify
    var arr = [];

    decirc(obj, '', [], undefined);
    var res = JSON.stringify(obj, replacer, spacer);
    while (arr.length !== 0) {
        var part = arr.pop();
        part[0][part[1]] = part[2];
    }
    return res;

    //////////// helpers

    function decirc (val, k, stack, parent) {
        var i;
        if (typeof val === 'object' && val !== null) {
            for (i = 0; i < stack.length; i++) {
                if (stack[i] === val) {
                    parent[k] = '[Circular]';
                    arr.push([parent, k, val]);
                    return;
                }
            }
            stack.push(val);
            // Optimize for Arrays. Big arrays could kill the performance otherwise!
            if (Array.isArray(val)) {
            for (i = 0; i < val.length; i++) {
                decirc(val[i], i, stack, val);
            }
            } else {
            var keys = Object.keys(val)
            for (i = 0; i < keys.length; i++) {
                var key = keys[i];
                decirc(val[key], key, stack, val);
            }
            }
            stack.pop();
        }
    }
};//end:safeStringify


AlfaJsonPretty.prototype.insertStyleTag = function(styleContainer){

    styleContainer = styleContainer || $dqs('head')

    var styleAttr = 'alfa-print-object-ui'+'-style'
    if(styleContainer.querySelector(`style[${styleAttr}]`)) return /* do not duplicate <style> tag */

    var styleTag = $dce('style');
    styleTag.setAttribute(styleAttr, '');

    styleTag.innerHTML = `
    [${ATTR}] {border: 1px solid silver}
    [${ATTR}] {border: unset}
    [${ATTR}] {font-size: 14px; margin: .4em 0}
    [${ATTR}].string { color: green; }
    [${ATTR}].number { color: rgb(139, 0, 0); }
    [${ATTR}].boolean { color: blue; }
    [${ATTR}].null { color: rgb(161, 1, 161); }
    [${ATTR}].key { color: rgb(0, 70, 161); }   
    `;

    /* NOTE: calling body.append will add your new styles to the bottom of the page and override any existing ones */
    styleContainer.append(styleTag);
}

export default AlfaJsonPretty;
