/*  array of items: [
        menu item without subitems: {text: {payloadObject}}
        menu item with subitems:    {text: [Array of subitems]}
    ]
*/

var menuData1 = [
    {'Alfa': [
            {'sub Alfa': {}}
        ]
    },
    {'Beta': [
        {'uno': {}},
        {'due': {}},
        {'tre': [{'Tretissimo': {}}]},
    ]},
    {'Gamma': {action: 'do something', rightIcon: 'i'}},
    {'Delta': {}},
    {'Disabled': {disabled: true}},
    {'Epsilon': {}},
    {'':{}},
    {'spacer above': {}},
    {'Url links': [
        {'index':    {url:'index.html'}},
        {'examples': {url:'examples.html'}},
        {'add css':  {url:'example-css.html'}}
    ]},
]

var menuData2 =  [
    {'1. One': [
        {'1.1 item': {}},
        {'1.2 item': [
            {'1.2.1':{}},
            {'1.2.2':{}},
            {'1.2.3':[
                {'1.2.3.1':{}},
                {'1.2.3.2':{}},
                {'1.2.3.3':{}},
                {'1.2.3.4':{}}
            ]}
        ]}
    ]},
    {'2. Two': [
        {'2.1. First subitem': {url: 'link'}},
        {'2.2. Second subitem': {url: 'link', rightIcon:'A'}},
        {'2.3. Third subitem': [
            {'2.3.1. uno ogetto': {url: 'link', rightIcon:'A'} },
            {'2.3.2. due ogetti':  [
                {'2.3.2.1. uz ': {url: 'link', rightIcon:'A'} },
                {'2.3.2.2. dos ':  [
                    {'2.3.2.2.1. kilenz ': {url: 'link', rightIcon:'A'} },
                    {'2.3.2.2.2. tiz ': {url: 'link'}},
                    {'2.3.2.2.3. wysonlataszlo ': {url: 'link'}}
                ]},
                {'2.3.2.3. trez ': {url: 'link'}}
            ]},
            {'2.3.3.tre ogetti': {url: 'link'}}
        ]},
        {'2.4. Fourth subitem': {url: 'link'}}
    ]},
    {'3. item Three': [
        {'3.1. jeden': {url: 'link', rightIcon:'A'} },
        {'3.2. dwa': {url: 'link', rightIcon:'A'} },
        {'3.3. trzy': {url: 'link', rightIcon:'A'} },
        {'3.4. cztery': {url: 'link', rightIcon:'A'} },
    ]},
    {'4. item Four with Lorem Ipsum':  [
        {'4.1. Lorem Ipsum Magna dolor veniam culpa id. Laboris qui proident aliqua id ipsum velit ex tempor. Id duis deserunt ex minim esse commodo ut esse excepteur ipsum velit. Ea in Lorem ex fugiat labore duis. Elit ea ex dolor aliqua mollit excepteur. Cupidatat esse aliqua qui ullamco qui id sit cupidatat nisi mollit laboris elit. Id eiusmod proident est aliquip eiusmod et minim labore amet incididunt cillum.':  {url: 'link', extraLink: true}},
        {'4.2 Multi line (done with html break)<br>second line<br>third line': {url:''}}
    ]},
    {'5. item Five':  {url: 'link', extraLink: true}}
];

export {menuData1, menuData2}