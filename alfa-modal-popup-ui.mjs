// alfa-modal-popup  9.5.8e    (c)alfalabs.net

// creates element dynamically, use only one instance per page

/** one instance on page
 *  for example collect-transaction.js has it's own and interferes with one from index.html
 */ 

/**
 alfaPopup.open(
    'HTML content of the message',
    'title bar text',   - optional, HTML string
    callbackFunction,   - optional, returns key pressed
    {                   - options
        modal   - boolean, can not be dismissed by clicking anywhere
        error   - boolean, displays red titlebar
        warn    - boolean, displays yellow titlebar
        audioClip = {
            error: 'errorDough',    - name of audio clip in alfaAudioclip
            warn: 'beep             - name of audio clip in alfaAudioclip
        } 
        cancel  - boolean, displays [Cancel] button
        noOK    - boolean, removes [OK] button
        hidden  - HTML string, hidden message revealed by ctr+atl click on title bar

        buttonsText = {
            ok:     'Yes'   - alternative text on buttons (html content)
            cancel: 'No'    - alternative text on buttons (html content)
        }
    }
)
 */


import AlfaConsoleLog from './alfa-console-log.mjs'; 
var alfaConsoleLog = new AlfaConsoleLog('alfa-modal-popup-ui')
var log =     function(){} // alfaConsoleLog.log.bind(alfaConsoleLog)
var logWarn = alfaConsoleLog.logWarn.bind(alfaConsoleLog)
var logErr =  alfaConsoleLog.logErr.bind(alfaConsoleLog)


// poor man's jQuery:
const $dqs = document.querySelector.bind(document); 
const $dce = document.createElement.bind(document); 

function AlfaModalPopup (){

    this.queue = [];

    this.uniqueTagAttr= 'alfa-modal-popup';

    this.insertStyleTag();

    this.create();
}

AlfaModalPopup.prototype.create = function(){  

    /* Layout:

    overlay - darkened, fixed, full screen
    popContainer - fixed, full screen
        popBox - centered in popContainer
            titlebar    - row
            popBody     - row
            popHidden   - row
            popFooter   - row
     */

    this.overlay = $dce('div');
    this.overlay.setAttribute('alfa-modal-popup-overlay','');
    this.overlay.setAttribute('style', 'display: none; position: fixed; left:0; top:0; bottom:0; right:0; background: black; opacity:.2;');
    $dqs('body').appendChild(this.overlay);

    this.popContainer =  $dce('div');   // popContainer is full screen as overlay
    this.popContainer.setAttribute(this.uniqueTagAttr,''); // make target for CSS to select by attribute
    this.popContainer.setAttribute('style', `
        display: none; 
        position: fixed; left:0; top:0; bottom:0; right:0; 
        justify-content: center; 
        align-items: center;`);
    this.popContainer.setAttribute('tabindex', '0');

    this.popContainer.onclick =   this.containerClickHandler.bind(this);
    this.popContainer.onkeydown = this.containerKeydownHandler.bind(this);

    this.popBox = $dce('div'); // centered popup box
    this.popBox.setAttribute('style', 'display:flex; flex-direction:column;  max-width:90%; max-height:85%; min-width:180px; color:black; background:white; box-shadow:2px 2px 4px #000000;');

    this.titlebar = $dce('div');

    this.popBody = $dce('div');
    this.popBody.setAttribute('style', 'padding: 20px; overflow-y: auto'); //  max-height: 85%;

    this.popHidden = $dce('div');
    this.popHidden.setAttribute('style', 'display:none; overflow:auto; padding: 0 5px; border-top: 1px solid silver; border-bottom: 1px solid silver;');

    this.popFooter = $dce('div');
    this.popFooter.setAttribute('style', 'display: flex; justify-content: center; padding: 7px 0 10px');

    this.btnOK = $dce('button');
    // this.btnOK.innerHTML = ' OK '; - to be set with current language
    this.btnOK.setAttribute('value', 'OK'); // this will survive translation to other languages
    this.btnOK.setAttribute('style', 'padding:0 15px;');
    this.btnOK.setAttribute('tabindex', '0');
    this.btnOK.onclick = this.close.bind(this);

    this.btnCancel = $dce('button');
    // this.btnCancel.innerHTML = 'Cancel'; - to be set with current language
    this.btnCancel.setAttribute('value', 'Cancel'); // this will survive translation to other languages
    this.btnCancel.setAttribute('style', 'display:none; padding:0 5px; margin-left: 10px;');
    this.btnCancel.setAttribute('tabindex', '0');
    this.btnCancel.onclick = this.close.bind(this);

    this.queueCount = $dce('div');   // to display multiple messages count
    this.queueCount.setAttribute('title', 'Message count');
    this.queueCount.setAttribute('style', 'display:none; width:25px; margin-top:-22px;  margin-left:3px;');

    this.popBox.appendChild(this.titlebar);
    this.popBox.appendChild(this.popBody);
    this.popBox.appendChild(this.popHidden);

    this.popFooter.appendChild(this.btnOK);
    this.popFooter.appendChild(this.btnCancel);
    
    

    this.popBox.appendChild(this.popFooter);
    this.popBox.appendChild(this.queueCount);

    this.popContainer.appendChild(this.popBox);
    $dqs('body').appendChild(this.popContainer);
   
};

AlfaModalPopup.prototype.buttonsText = function(opts){

    /** alternate text on buttons */
    if(opts.buttonsText){
        this.btnOK.innerHTML =     opts.buttonsText.ok
        this.btnCancel.innerHTML = opts.buttonsText.cancel
    } else {
        this.btnOK.innerHTML = ' OK '
        this.btnCancel.innerHTML = 'Cancel'
    }
}

AlfaModalPopup.prototype.open = function(htmlContent, title, callback, opts){

    opts = opts || {};

    if(this.isOpen){this.queueAdd({htmlContent, title, callback, opts}); return;}

    this.isOpen = true;

    this.modal = typeof opts.modal==='undefined' ? true : opts.modal; // default: true

    /** check if any element is on top of alfa-modal-popup */
    if(this.popContainer.nextSibling){console.warn('<alfa-modal-popup> may be obsucred by element:', this.popContainer.nextSibling)}

    this.overlay.style.display = 'block';
    this.popContainer.style.display = 'flex';

    if(opts.modal) {this.titlebar.classList.add('titlebar-modal'); }
    if(opts.error) {this.titlebar.classList.add('titlebar-error');} else {this.titlebar.classList.remove('titlebar-error');} 
    if(opts.warn)  {this.titlebar.classList.add('titlebar-warn');}  else {this.titlebar.classList.remove('titlebar-warn');} 

    if(opts.cancel){this.btnCancel.style.display = 'block';}
    if(opts.noOK)  {this.btnOK.style.display = 'none';}
    if(opts.hidden){
        this.popHidden.innerHTML = opts.hidden;
        this.titlebar.onclick = function(ev){
            ev.stopImmediatePropagation();
            if(ev.ctrlKey && ev.altKey){this.popHidden.style.display='block';}
        }.bind(this);
    }

    this.audio(opts);
    // if(opts.audioClip && window.alfaAudioClips){window.alfaAudioClips(opts.audioClip)}

    if(title) {this.titlebar.innerHTML = title.startsWith('<') ? title : ('&nbsp;'+ title) } 
    this.popBody.innerHTML = htmlContent;
    this.callback=callback;

    if(opts.noOK) {
        this.popContainer.focus();// focus has to be somewhere here or pressing Enter will create new instances of popup
        this.popContainer.addEventListener('keydown', function(ev){ev.stopImmediatePropagation(); if(ev.keyCode===13){this.close(ev);}}.bind(this));
    } 
    else this.btnOK.focus(); 

    this.buttonsText(opts)
};

AlfaModalPopup.prototype.audio = function(opts){
    if(!window.alfaAudioClips) return;

    if(opts.audioClip){
        window.alfaAudioClips(opts.audioClip, null, {volume:.8})
    } else {
        if(opts.error) {window.alfaAudioClips('errorDough', null, {volume:.8}); } 
        if(opts.warn)  {window.alfaAudioClips('beep',  null, {volume:.3}); } 
    }   
}

AlfaModalPopup.prototype.close = function(ev){

    ev.stopImmediatePropagation();

    log('close()', ev.target.value);

    this.isOpen = false;

    this.overlay.style.display = 'none';
    this.popContainer.style.display = 'none';

    this.titlebar.innerHTML = '';
    this.popBody.innerHTML = '';

    // cleanup
    this.modal = false;
    this.popBox.classList.remove('get-attention');
    this.titlebar.classList.remove('titlebar-error');
    this.titlebar.classList.remove('titlebar-modal');
    this.popHidden.style.display = 'none';
    this.btnOK.style.display = 'block';
    this.btnCancel.style.display = 'none';
    //

    if (typeof this.callback==='function') this.callback(ev.target.value); // value attribute is in English to survive translation of btn text
    
    this.queueGet();
};

AlfaModalPopup.prototype.queueAdd = function(item){
    log('queueAdd()', this.queue.length);
    if(item.callback){logWarn(`message '${item.htmlContent.substring(0,20)}...' has callback!`);}
    this.queue.push(item);
    this.queueCount.style.display = 'block';
    this.queueCount.innerHTML = `${this.queue.length+1}`;
};
AlfaModalPopup.prototype.queueGet = function(){
    log('queueGet() remaining:', this.queue.length);
    if (this.queue.length>0){
        this.queueCount.innerHTML = `${this.queue.length}`;
        var item = this.queue.shift();
        this.open(item.htmlContent, item.title, item.callback, item.opts);
    } else {
        this.queueCount.style.display = 'none';
    }
};

AlfaModalPopup.prototype.containerClickHandler = function(ev){
    // ev.stopImmediatePropagation();
    // ev.stopPropagation();

    if(this.modal) {this.popBox.classList.add('get-attention'); return;}
    ev.target.value='container';
    this.close(ev);
};
AlfaModalPopup.prototype.containerKeydownHandler = function(ev){
    if (this.modal) return;
    this.close(ev)
}
// AlfaModalPopup.prototype.enterKeyHandler = function(ev){
//     if (ev.keyCode === 13) {this.close('Enter');}
// };


/** only needed for Error to have different style */
AlfaModalPopup.prototype.insertStyleTag = function(){
    var styleTag = $dce('style');
    styleTag.setAttribute(this.uniqueTagAttr+'-style', '');

    //NOTE: div > div:nth-child(1) {padding: 3px 3px 0 3px;  causes color bleed
    styleTag.innerHTML = `
    div[${this.uniqueTagAttr}] > div > div:nth-child(1) {background: silver;}
    div[${this.uniqueTagAttr}] .titlebar-modal {background:#0043ff !important; color: white;} 
    div[${this.uniqueTagAttr}] .titlebar-error {background:red !important; color: white;} 
    div[${this.uniqueTagAttr}] .titlebar-warn {background:darkorange !important; color: white;} 
    div[${this.uniqueTagAttr}] .get-attention {outline: 5px solid orange;}
    `;

    $dqs('head').append(styleTag);
};

export default AlfaModalPopup

    // helpers
    // function log(){
    //     return;
    //     var args = Array.prototype.slice.call(arguments); 
    //     args.unshift('%c[alfa-popup]', 'color:blue');
    //     console.log.apply(null, args);
    // }
    // function logWarn(){
    //     var args = Array.prototype.slice.call(arguments); 
    //     args.unshift('%c[alfa-popup] WARNING:', 'color:red; font-weight:bold;');
    //     console.log.apply(null, args);
    // }