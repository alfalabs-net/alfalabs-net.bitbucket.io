/** alfa-index-ui.mjs 
 * 
 *  columns in CSV index-list file
 *  [0] name
 *  [1] type
 *  [2] NodeJS
 *  [3] description
 *  [4] optional
 *  [5] used by callback makeLinkHref()
*/

const $dce = document.createElement.bind(document);
const $dqs = document.querySelector.bind(document);



function AlfaIndexUI (list, opts){

    this.cfg = opts || {}
    this.cfg.defaultIndexHtml = this.cfg.defaultIndexHtml || 'index.html'

    this.list = convertCSVLines(list)

    /** convert from comma delimetered to Array */
    function convertCSVLines(list){
        var lines = list.split('\n')

        var rows = []
        lines.forEach(function(line) {
            if(line!==''){
                /** line is not a spacer 
                 *  convert CSV to array                */
               rows.push(makeRow(line))
                
            } else {
                rows.push('') /** spacer */
            }
        });
        ///
        function makeRow(row){
            row = row.split(',')
            row.forEach(function(el, i){
                this[i] = el.trim()
            }, row)
            return row
        }
        // console.log('convertCSVLines', rows)
        return rows;
    }
    

}

AlfaIndexUI.prototype.create = function(){
    
    this.insertStyleTag()
    var ul = $dqs('#alfa-index-ui')
    var makeLinkHref = this.cfg.makeLinkHref;
    var defaultIndexHtml = this.cfg.defaultIndexHtml;

    /** create <li> elements */
    this.list.forEach(function(row) {
        if(row!==''){
            var li = $dce('li');
            li.innerHTML = makeRow.call(this, row)
            ul.append(li)
        } else {
            var div = $dce('div');
            div.style = 'height: .5em';
            ul.append(div)
        }
    }.bind(this))


    function makeRow(row){

        var name = row[0]
        var type = row[1] || ''
        var desc = row[3] || ''
        var node = makeNodeLink(row[2], name)
        var opts = makeOpts(row[4])
        if(opts.disabled && !this.showDisabled) return ''

        var htm = `
        <div class="col name" ${opts.disabled ? 'disabled': ''}><a href="${makeLinkHrefFn(row)}" target="_blank">${name}</a></div>
        <div class="col type">${type}</div>
        <div class="col node">${node}</div>
        <div class="col desc">${desc}</div>
        `
        return htm
    }

   
    function makeLinkHrefFn(row){
        if(makeLinkHref){ /** callback to make non standard link */
            return makeLinkHref(row)
        } else {
            return `./${row[0]}/${defaultIndexHtml}`
        }
    } 

    function makeNodeLink(nodeInfo, appName){
        if(!nodeInfo) return ''

        /** usual node route:
         *      /node/APP-NAME/:cmd/:value?
         * in column row[2]
         *      /node/:cmd/:value
         */
        var arr = nodeInfo.split('/')
        var node = arr[0] // should be 'node'
        var cmd = arr[1]
        var value = arr[2]

        return `<a href="/node/${appName}/${cmd}" target="_blank" title="NodeJS">&#x2B22;</a>` 
    }
    function makeOpts(opt){
        if(!opt) return {}
        return JSON.parse(opt)
    }

}
AlfaIndexUI.prototype.clear = function(){
    var myNode = $dqs('#alfa-index-ui')
    while (myNode.firstChild) {
        myNode.removeChild(myNode.lastChild);
    }
}

AlfaIndexUI.prototype.disabledShow = function(){
    this.showDisabled = !this.showDisabled
    this.clear()
    this.create()
}

AlfaIndexUI.prototype.sort = function(){

    /** remove empty lines */
    var arr1 = this.list.filter(function(item){
        return /\S/.test(item);
        /** \S means "a non-whitespace character," so /\S/.test(...) checks if a string contains at least one non-whitespace char. */
    })

    this.sortDown = typeof this.sortDown==='undefined' ? this.sortDown = true : this.sortDown = !this.sortDown;

    var sorted = arr1.sort(function(a, b){
        /** sort by first column */
        var nameA = a[0].toUpperCase(); // ignore upper and lowercase
        var nameB = b[0].toUpperCase(); // ignore upper and lowercase
        
        if(this.sortDown){
            if (nameA < nameB) { return -1; }
            if (nameA > nameB) { return  1;} 
        } else {
            if (nameA < nameB) { return  1; }
            if (nameA > nameB) { return -1;} 
        }

        return 0; /* names must be equal */
    }.bind(this))

    // console.log('sorted', sorted)
    this.list = sorted;

    this.clear()
    this.create()
}

AlfaIndexUI.prototype.insertStyleTag = function(){

    var styleTag = $dce('style');
    styleTag.setAttribute('alfa-index-ui'+'-style', '');
    var sel = '#alfa-index-ui';

    styleTag.innerHTML = `
        ${sel} [disabled] > a {color: grey;}
        ${sel} {list-style-type: none;  padding-left: 1em;}
        ${sel} li{border-bottom: 1px solid gainsboro}

        ${sel} a{text-decoration: none;}
        ${sel} a:hover{color:darkmagenta; font-weight:bold}

        /* columns */
        ${sel} .col{display:inline-block}
        ${sel} .name{min-width: 11em; font-size: 17px;}
        ${sel} .type{min-width:  1em; font-size: 14px}
        ${sel} .node{min-width: 1em; font-weight:bold;}
        ${sel} .desc{font-size: .8em}
    `;

    /* NOTE: calling body.append will add your new styles to the bottom of the page and override any existing ones */
    $dqs('head').append(styleTag);
}


export default AlfaIndexUI;