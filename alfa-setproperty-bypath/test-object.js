var testObj = {
    dsApp:{
        alfa: 'A',
        beta: {one: 1, two: false}
    },
    dsPage: {
        gamma: {
            delta: 'D',
        }
    },
    arrayOne: [
        {one: 1},
        {two: 'deux'},
    ]
}

export {testObj}