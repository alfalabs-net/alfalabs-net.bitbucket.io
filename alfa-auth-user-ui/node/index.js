/** this module has to be customized to APP using it */

const AlfaAuth = require('./alfa-auth.js')

module.exports = function(req, res, next){ 

    var alfaAuth = new AlfaAuth({
        userList: require('./alfa-auth-users'), /** this is specific to APP, where users are located */
    })

    if(req.params && req.params.cmd){ 
        switch (req.params.cmd){
            case 'verify': alfaAuth.verifyJWToken(req, res, next); break
            default: alfaAuth.authenticateUser(req, res)
        }
    } else {
        alfaAuth.authenticateUser(req, res)
        // setTimeout(function(){ authenticateUser((req, res) }, 2000) - to test wait spinner
    }
}
