'use strict'
const jwt = require('jsonwebtoken')
const SECRET_KEY = 'alfa156';
const EXPIRES_IN = '20h';         // expressed in seconds or a string describing a time span zeit/ms. 

const MODULE_NAME='alfa-auth'

/** reusable module
 * 
 *  use example:
        const AlfaAuth = require('./alfa-auth.js')
        var alfaAuth = new AlfaAuth({
            userList: require('./alfa-auth-users'), /** this is specific to APP, where users are located * /
        })
 */

class AlfaAuth {
  
    /**
     * @param {*} settings - userList - where users {list, cfg} module is
     */ 
    constructor(settings={}){
        Object.assign(this, settings)
    }

    authenticateUser(req, res){ /** and create JWToken */
        // log(`[${MODULE_NAME}] authenticateUser()`, req.body)

        var RESULT = { MODULE_NAME }
        var coreCallbacks = new CoreCallbacks(req, res , {MODULE_NAME})
        var reqBody = req.body

        var userCreds = reqBody.userCredentials
        if(!userCreds){
            RESULT.error = {message: 'userCredentials are missing!'}
            coreCallbacks.sendResutl(RESULT)
            return 
        }
        var hash_type = reqBody.hash_type
        hash_type = hash_type ? hash_type : ''
        var cfg_hash_type = this.userList.cfg.hash_type ? this.userList.cfg.hash_type : ''
        if(hash_type !== cfg_hash_type){
            RESULT.error = {message: 'Hash type not matching!', extra:`expected: '${cfg_hash_type}' received: '${hash_type}'`}
            coreCallbacks.sendResutl(RESULT)
            return
        }

        var found = this.userList.list.find(function(user){
            if(userCreds.user_email) return user.user_email === userCreds.user_email && user.user_pass === userCreds.user_pass
            else                     return user.user_id    === userCreds.user_id    && user.user_pass === userCreds.user_pass
        })

        if(found){
           
            RESULT.error = null;
            this.createJWToken(null, function(err, token){
                if(err){/* console.error already shown */ RESULT.error = err}
                RESULT.accessToken = token;
                RESULT.userInfo = Object.assign({}, found) 
                delete RESULT.userInfo.user_pass /* do not return password hash */
                coreCallbacks.sendResutl(RESULT);
            });
        } else {
            RESULT.error = {message:'User name or password not found.'};
            coreCallbacks.sendResutl(RESULT);
        }
    }

    createJWToken(payload, cb){
        payload = payload || {};
        jwt.sign({payload}, SECRET_KEY, { expiresIn: EXPIRES_IN }, (err, token) => {
            // err={message:'test error in createJWToken()'}// forDEBUG
            if (err) {
                console.error('createJWToken()', err.name, err.message); 
                cb(err);
            } else {
                cb(null, token);}
        });
    }

    verifyJWToken(req, res, next){
        var token = req.headers['x-access-token'];

        jwt.verify(token, SECRET_KEY, function(err, decoded) {
            if (err){ 
                res.sendStatus(403); // works OK
                // log('verify(403) Forbidden', info);
                // // NOT WORKING:
                // res.redirect('/auth-failed.html'); 
                // or
                // res.writeHead(301, {Location: '/auth-failed.html'} );
                // res.end();
            } 
            else    { 
                if (typeof next==='function'){ next(); } /* for router to access routes below */
                else { res.sendStatus(277); }
            }
        });
    }
}



/** to be compatible with alfa-core coreCallbacks.js */
class CoreCallbacks{
    constructor(req, res , {MODULE_NAME}){
        Object.assign(this, {req, res, MODULE_NAME})
    }
    sendResutl(result){
        if (this.res){
            if (this.res.headersSent){console.error('headersSent! res:', this.res);}
            else this.res.send(result);
        } else {
            log(`${this.MODULE_NAME} sendResult() non Http request, result:`, result); // forDEBUG only !
        }
    }
}

module.exports = AlfaAuth

function log(){
    // var args = Array.prototype.slice.call(arguments); // not working in strict mode? but works here
    // args.unshift(`%c${MODULE_NAME}`, `color:magenta`);
    console.log.apply(null, arguments);
}