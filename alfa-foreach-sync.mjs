    /*! Copyright 2020, AlfaDigital.NET Inc.  MIT License    */
    
    /** foreachSync         
     * 
     *  @param {Array} arr - an array to enumerate
     *  @param {Object} callbacks   
     *              onBefore(i, cb), - before starting item processing @optional
     *              onEach(i, cb),   - receiving item processing callback
     *              onError(i, msgObj),  - @optional
     *              onProgress(i, msgObj),  - msgObj.status: OK, late @optional
     *              onDone(errors)          - @optional
     *              isCancelled()   - @optional see if async loop is cancelled by user
     *  @param {object} opts - options 
     *              timeout - in ms    
     *              errorsToStop - count of errors to abort array iteration   
     */ 
    var foreachSync = function(arr, callbacks, opts){

        /* required callback */
        if(typeof callbacks.onEach!=='function'){console.error('foreachSync() callback onEach is not a function!', opts); return}
        
        /* optional callbacks */
        callbacks.onProgress = callbacks.onProgress || function(){};
        callbacks.onBefore =   callbacks.onBefore || function(){};
        callbacks.onError =    callbacks.onError || function(){};
        callbacks.onDone =     callbacks.onDone || function(){};
        callbacks.isCancelled= callbacks.isCancelled || function(){};

        opts = opts || {};
        /** defaults */
        var timeout = opts.timeout ? opts.timeout : 25000;
        var errorsToStop = opts.errorsToStop ? opts.errorsToStop : 10;

        var setTmIDs = [];  /* store setTimeout IDs  */
        var errors = [];    /* store timeout errors */
        var warnings = [];  /* store "arrived after" */
        var times = [];     /* store time for each iteration process */
        var startTime = Date.now();
        

        recursive(0);


        function recursive(i, next){

            if(callbacks.isCancelled()) {
                var message = 'Cancelled after '+i+' iterations.'
                callbacks.onProgress(i, {status: 'Cancelled', message, time: times[currIndex]});
                callbacks.onDone(i, {status: 'cancelled', time:  Date.now() - startTime, errors, warnings});
                // i = arr.length;
                return;
            }

            var currIndex = i;

            if(i < arr.length){

                /* 0. check error count */
                if(errors.length >= errorsToStop) {
                    callbacks.onDone(i, {status:'aborted', message: 'error count reached max allowed: '+errorsToStop+', aborting!', errors, warnings}); 
                    return;} /* too many errors, aborting */

                /* A. before starting item process */
                callbacks.onBefore(i);
                
                /* 1. call async function  */
                callbacks.onEach(
                    i,              /* index */
                    function(err){  /* next callback */
                        
                        /* we have response, check errors, if it is not after timeout, continue recursing */
                        if(checkErrors(currIndex, err)){
                            
                            /** check if Sync function is used, 
                             *  the value of 'i' will not be increased because Sync function runs in the same stack */
                            if(i===currIndex){
                                console.error('foreachSync() onEach has to be ASYNC function!', {i, currIndex})
                                i++; /* prevent infinite loop */
                            }

                            /* NOTE: i is already increased in step 3, it is not the same that was passed to Async onEach()  */
                            recursive(i, next)
                        }
                })

                /* 2. start timeout countdown for this recursion pass  */
                startTimeout(currIndex)

                /* 3. increase index    */
                i++;

            } else {
                /*  iteration finished */
                callbacks.onDone(i, {status: 'done', time:  Date.now() - startTime, errors, warnings})
            }
        }

        
        /** timeout handling    ********************** */     
        
        function startTimeout(i){
            times[i] = Date.now()

            setTmIDs[i] = setTimeout(function(){
                /* inform about timeout */
                var message = 'TIMEOUT '+timeout/1000+'s';
                callbacks.onProgress(i, {status: 'timeout', message})
                callbacks.onError(i, {message})
                errors.push({index: i, message})

                /* remove timeout from list */
                setTmIDs[i] = null;

                /* continue with next item  */
                recursive(i+1)

            }, timeout, i)
        }
        /** check timeout and errors */
        function checkErrors(currIndex, err){

            /* we have response,  */
            times[currIndex] = Date.now() - times[currIndex];
                    
            /* check if response is after timeout   */
            if(setTmIDs[currIndex]===null) {
                var message = 'Response: '+times[currIndex]+'ms arrived after timeout. Rejected!' ;
                callbacks.onProgress(currIndex, {status: 'late', message, time: times[currIndex]})
                warnings.push({index: currIndex, message})
                /** stop recursing   */
                return false

            } else {

                /*  check if errors have arrived    */
                if(err){
                    var message = err.message || 'responded with Error';
                    callbacks.onProgress(currIndex, {status:'ERR', message, time: times[currIndex]})
                    callbacks.onError(currIndex, {message}, err)
                    errors.push({index: currIndex, message})
                } else {
                    /*  OK   */
                    callbacks.onProgress(currIndex, {status:'OK', message:'responded', time: times[currIndex]})
                }
                
                /*  clear timeout for this pass     */
                clearTimeout(setTmIDs[currIndex])

                /** continue recursing */
                return true
            }
        }
    }//end: foreachSync ------------------------------------------------------
        

    export default foreachSync;