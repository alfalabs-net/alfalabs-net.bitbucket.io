var testObj = {
    dsApp:{
        alfa: 'A',
        beta: 'B',
        gamma: {one: 1, two:2}
    },
    dsPage: {
        gamma: {
            uno: 1,
            due: 2
        }
    }
}

var testObjBig = [
       
    { text: '<span style="color:#bd0d00">AlfaScript</span> <span style="color:#006100">Node_API scripting language</snap>', payload: [
        { text: 'Events', payload: [
            { text: 'on Page', payload: [
                { text: 'on_ready - page is built',      payload: { frm: 'events-submit-cancel' } },
                { text: 'on_open - page is in viewport', payload: { frm: 'events-submit-cancel' } },
                { text: 'on_cancel - button handler',    payload: { frm: 'events-submit-cancel' }, disabled:true  },
                { text: 'on_sections_created', disabled:true }
            ]},
            { text: 'on Section', payload: [
                { text: 'on_active', payload: { frm: 'events-submit-cancel' } },
                { text: 'on_submit', payload: { frm: 'events-submit-cancel' } },
                { text: 'on_cancel', payload: { frm: 'events-submit-cancel' } },
            ]},
            { text: 'on Transaction', payload: [
                { text: 'on_response', payload: { frm:  'action-transaction-on-load'} },
                { text: 'on_error',    payload: { frm:  'action-transaction-on-load'} },
            ]},
        ]},
        { text: 'Actions', payload: [
            { text:'on SECTIONS', payload:[
                { text: 'section_clear', payload: { frm: 'alfa-checkbox' } },
                { text: 'goto_section', payload: { frm: 'section-goto' } },
                { text: 'next_section', payload: { frm: 'actions-go' } },
            ]},
            { text:'on PAGES', payload:[
                { text: 'goto_page', payload: { frm: 'action-goto_page' } },
                { text: 'goto_calling_page', payload: { frm: 'action-goto_page' } },
                { text: 'goto_calling_page (imported)', payload: { frm: 'aa-import-common' } },
            ]},
            { text:'goto-somewhere', payload:[
                { text: 'goto_field',   payload: { frm: 'section-goto' } },
                { text: 'goto_section', payload: { frm: 'section-goto' } },
                { text: 'next_section', payload: { frm: 'actions-go' } },
                { text: 'goto_page', payload: { frm: 'action-goto_page' } },
                { text: 'goto_calling_page', payload: { frm: 'action-goto_page' } },
                { text: 'goto_calling_page (imported)', payload: { frm: 'aa-import-common' } },
                { text: 'goto_applet',           payload: { frm: 'action-goto_applet' } },
                { text: 'goto_calling_applet',   payload: { frm: 'action-goto_applet' } },
                { text: 'goto_previous_applet',  payload: { frm: 'action-goto_applet' } },
                { text: 'goto_previous_or_menu', payload: { frm: 'action-goto_applet' } },
                { text: 'goto_applets_menu',     payload: { frm: 'action-goto_applet' } },
                { text: 'reload_applet',         payload: { frm: 'action-goto_applet' } },
            ]},
            { text: 'transaction', payload: [
                { text: 'on page load', payload: { frm: 'action-transaction-on-load' } },
                { text: 'on submit',    payload: { frm: 'action-transaction-on-submit' } },
                { text: 'two async (simultaneous)', payload: { frm: 'action-transactions-async' } },
                { text: 'two chained (one after another)', payload: { frm: 'action-transactions-chained' } },
                { text: 'request PARAMs', payload: [
                    { text: 'request_params', disabled:'true'},
                    { text: 'request_params_byvalue', disabled:'true'},
                ]},
            ]},
            // { text: 'goto (section, page, applet), next', payload: { frm: 'actions-go' } },
            { text: 'run_function', payload: { frm: 'action-run_function' } },
           
            { text: 'condition_if', payload: [
                { text: 'is_true', payload:  { frm: 'action-condition' } },
                { text: 'is_true_bool_by_path', payload:  { frm: 'action-condition' } },
                { text: 'is_equal', payload: { frm: 'action-condition' } },
                { text: 'is_equal_by_path', payload: { frm: 'action-condition' } },
                { text: 'is_equal_numeric_by_path', payload: { frm: 'action-condition' } },
                { text: 'is_empty', payload: { frm: 'action-condition' } },
                { text: 'is_zero', payload:  { frm: 'action-condition' } },
            ]  },
            // { text: 'Enable/Disable fields', payload: { frm: 'actions-able_fields' } },
            // { text: 'data_elem commands', payload: { frm: 'actions-data_element-cmd' } },
            // { text: 'row2array', disabled:true, payload: { frm: 'actions-row2array' } },
            { text: 'console_log',   payload: { frm: 'action-console_log' } },
            { text: 'console_log_value',  payload: { frm: 'action-console_log' } },
            { text: 'console_log_value_async - <i>for testing</i>',  payload: { frm: 'action-foreach' } },
            { text: 'set_values',     payload: { frm: 'action-set_values' } },
            { text: 'assign_byvalue', payload: { frm: 'action-set_values' } },
            { text: 'delete_values', payload: { frm: 'data-binding' } },
            { text: 'foreach', payload: { frm: 'action-foreach' } },
            { text: 'foreach_sync', payload: { frm: 'action-foreach' } },
            { text: 'popup_message', payload: { frm: 'action-popup_message' } },
            
        ]},
        { text: 'Functions', payload: [
            { text: 'concatenate( )', payload:   { frm: 'imported-functions' } },
            { text: 'calculate( )', payload:     { frm: 'imported-functions' } },
            { text: 'calculateBool( )', payload: { frm: 'imported-functions' } },
            { text: 'calculate( ) with condition_if', payload: { frm: 'action-condition' } },
            { text: 'lookupListRequest( )', payload: { frm: 'fn-lookup-list-request' } },
            { text: '&#10094;alfa-table&#10095; functions', payload: [
                { text: 'addRecord( )', payload: { frm: 'alfa-table-generated' } },
                { text: 'removeSelectedRecord( )', payload: { frm: 'alfa-table-generated' } },
                { text: 'updateSelectedRecord( )', payload: { frm: 'alfa-table-generated' } },
            ]},
            { text: 'ruleString( )', payload:   { frm: 'fn-rules' } },
            { text: 'ruleBoolean( )', payload:   { frm: 'fn-rules' } },
            { text: 'ruleNumber( )', payload:   { frm: 'fn-rules' } },
        ]},
        { text: 'Commands (buttons and menu)', payload: { frm: 'menu-commands' } },
      
        { text: '&#10094;alfa-elements&#10095;', payload: [
            { text: '&#10094;alfa-input&#10095;',  payload: { frm: 'alfa-input' } },
            { text: '&#10094;alfa-span&#10095;',   payload: { frm: 'alfa-span' } },
            { text: '&#10094;alfa-table&#10095;',  payload: { frm: 'alfa-table' } },
            { text: '&#10094;alfa-table&#10095; functions', payload: [
                { text: 'addRecord()', payload: { frm: 'alfa-table-generated' } },
                { text: 'removeSelectedRecord()', payload: { frm: 'alfa-table-generated' } },
                { text: 'updateSelectedRecord()', payload: { frm: 'alfa-table-generated' } },
            ]},
            { text: '&#10094;alfa-lookup&#10095;', payload: { frm: 'alfa-lookup' } },
            { text: '&#10094;alfa-lookup&#10095; lookupListRequest()', payload: { frm: 'fn-lookup-list-request' } },
            { text: '&#10094;alfa-button&#10095;', payload: { frm: 'alfa-button' } },
            { text: '&#10094;alfa-progress&#10095;', payload: { frm: 'alfa-progress' } },
            { text: '&#10094;alfa-progress&#10095; with foreach_sync', payload: { frm: 'action-foreach' } },
            { text: '&#10094;alfa-checkbox&#10095;', payload: { frm: 'alfa-checkbox' } },
            { text: '&#10094;alfa-group&#10095;',   payload: { frm: 'alfa-group' }, },
            { text: '&#10094;alfa-element&#10095; examples', payload: { frm: 'alfa-element-examples' }, disabled:true },
            { text: '&#10094;alfa-input-linked&#10095;', payload: { frm: 'alfa-input-linked' }, disabled:true },
            { text: '&#10094;alfa-section&#10095;',   payload: { frm: 'alfa-section' }, },
            { text: '&#10094;alfa-section&#10095; focus',   payload: { frm: 'section-focus' }, },
            { text: 'Styling alfa-elements with CSS', payload: { frm: 'alfa-element-css' } },
            { text: 'alfa-elements HTML content', payload: [
                { text: 'fixed_value'},
                { text: 'inner_text'},
                { text: 'inner_html'},
            ] },
           
        ]},
        { text: 'Data Source scope', payload: [
            { text: 'dsPage',  payload: { frm: 'ds-app-one' } },
            { text: 'dsApp',  payload:  { frm: 'ds-app-one' } },
            { text: 'dsGlob',  payload: { frm: 'ds-app-one' } },
            { text: 'pass params from menu',  payload: { frm: 'ds-app-one', params: {fromMenu: 'param from menu'} } },
            { text: 'ds-app-two',  payload: { frm: 'ds-app-two' } },
        ]},
        { text: 'Data Binding path types', payload: [
            { text: 'bound_path',   },
            { text: 'bound_path_out',   },
            { text: 'bound_path_disabled',   },
            { text: 'bound_path_disabled_onfalse',   },
            { text: 'bound_path_default',   },
            { text: 'bound_path_settings', disabled:true   },
            { text: 'bound_path_select_index <i style="color:#666">for &lt;alfa-table&gt;</i>',   },
            { text: 'bound_path_cell_css     <i style="color:#666">for &lt;alfa-table&gt;</i>',   },
            { text: 'bound_path_cell_html    <i style="color:#666">for &lt;alfa-table&gt;</i>',   },
            { text: 'bound_path_css',   },
        ]},
        { text: 'Importing pages, sections. etc', payload: [
            { text: 'Importing from COMMON',       payload: { frm: 'importing-pages-sections'}  },
            { text: 'Importing from other-applet', payload: { frm: 'importing-pages-sections2'}  },
            { text: 'other-applet used in example above', payload: { frm: 'importing-other-applet'}  },
        ]},
        { text: 'Navigation between Applets', payload: [
            { text: 'reload_applet',         payload: { frm: 'action-goto_applet' } },
            { text: 'goto_applet',           payload: { frm: 'action-goto_applet' } },
            { text: 'goto_calling_applet',   payload: { frm: 'action-goto_applet' } },
            { text: 'goto_previous_applet',  payload: { frm: 'action-goto_applet' } },
            { text: 'goto_previous_or_menu', payload: { frm: 'action-goto_applet' } },
            { text: 'goto_applets_menu',     payload: { frm: 'action-goto_applet' } },           
        ]},
        { text: 'Testing, Examples and Experimantal', payload: [
            { text: 'Applet template: AA-TEMPLATE', payload: { frm: 'aa-template', params: {super: true, a:123, b:null} } },
            { text: 'CSS layout',      payload: { frm: 'test-css-layout' } },
            { text: 'Section example', payload: { frm: 'test-section-example' } },
            { text: '&#10094;alfa-drawer&#10095; Menu Drawer', payload: { frm: 'menu-drawer' } },
            { text: 'notifySubscribers()',      payload: { frm: 'test-notify-subscribers' } },
            { text: 'alfa-mixin-databinding',      payload: { frm: 'data-binding' } },
            { text: 'fn-rules',      payload: { frm: 'fn-rules' } },
            { text: 'msc-test-transactions', payload: { frm: 'msc-test-transactions' } },

        ]}
    ]},
    { text: 'Login to Collect', payload: { url: '/msc-collect-transaction/index.html' },  },
   
    { text: 'About', payload: { url: '/about.html' }, disabled: false }
];


export {testObj, testObjBig}