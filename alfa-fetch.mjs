/*! alfa-fetch.mjs  9.8.14a2   es6 module Copyright 2016 AlfaDigital.NET Inc, License MIT */
/*
    each alfaFetch() call uses separate instance of AlfaFetch

    usage:
        import alfaFetch from '/ALFA_es6modules/alfa-fetch.mjs'; /** this is the instance of AlfaFetch, ready to use * /
        window.alfaFetch = alfaFetch;

    optionally
        import AlfaModalSpinner from './alfa-modal-spinner-ui.mjs';
        window.alfaModalSpinner = new AlfaModalSpinner();
*/
export default function alfaFetch(url, options){ new AlfaFetch(url, options);}  // global on page


var alfaFetchInstances = {}; // stores instances, to be used by this.dispose()  global on page


function AlfaFetch (url, options){

    this.defaults = { // this property can be seen outside to find sttings, do not change to var
        method: 'GET',
        timeout: 45000,  // initial response may be long
        contentType: 'application/json',
        // spinnerWidget: typeof window.alfaModalSpinner !=='undefined' ? window.alfaModalSpinner : null,  // jQuery widget or ms6 module, loaded on page, expected to have .on() and .off() methods
        responseElementId: 'fetch-response',    // for build in response testing
        errorElementId:    'fetch-error',        // for build in response testing
        counter: null                           // to send in http header x-counter
    };
    options = options || {};
    Object.assign(this, this.defaults, options);

    if (typeof options.getInstance === 'function') {options.getInstance(this); this.dispose(); return;}

    this.url = url;
    this.requestId =  this.uid(); // this.uuid();
    alfaFetchInstances[this.requestId] = this;
    // alfaFetchInstancesCount++;
    // if (!this.spinnerWidget){log('spinnerWidget is falsy.');}
    
    this.response = {}; // {responseMeta, responseData, responseError}


    // this.timeOut = new this.timeOutConstructor(); - performs better if constructed inside makeRequest() ?

    this.makeRequest(url);

}
AlfaFetch.prototype.makeRequest = function (url){
    
    // var that = this;

    // for debugging, show request url to hard coded element on page
    var debugBox = document.getElementById('alfa-fetch-request-url');
    if (debugBox) debugBox.innerHTML = `<b>${this.method}</b>:  ${url}`;

    this.timeOut = new this.timeOutConstructor(); // constructed here makes better performance ?
    this.timeOut.start.call(this);
    this.spinner('on');

    log('0', this.requestId, url, this);
    if(typeof this.onStart==='function') this.onStart(null, this.requestId); // it can be used to start external spinner, first arg=null is spinner time

    var CURRENT_URL = url; // testing closure
    

    fetch(url, {
            method: this.method,
            body: this.body ? JSON.stringify(this.body) : null,
            // mode: "cors", // no-cors, cors, *same-origin
            // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: "same-origin", // include, *same-origin, omit
            
            // headers: {
            //     'Content-Type': this.contentType,
            //     'x-request-id': this.requestId, // "access-control-allow-headers": "x-request-id",
            //     'x-access-token': sessionStorage.getItem('x-access-token')
            //     // "x-counter": this.counter
            // }

            headers: this.headers ? this.headers : {
                'Content-Type': this.contentType,
                'x-request-id': this.requestId, // "access-control-allow-headers": "x-request-id",
                'x-access-token': sessionStorage.getItem('x-access-token')
                // "x-counter": this.counter
            }
        })
    .then(function(response) {
        /* stage 1 - response does have a payload in Promise */
        // log('1', this.requestId);

        if (this.timeOut.end()===false) return null; // if timeout is exceeded, returns false

        // make error to go to .catch()     
        // this.propOfUndefined = 1;

        if(response.ok) {
            this.response.responseMeta = this.getMetaData(response); // at first, response contains metadata
            switch (this.contentType){
                case 'application/json': return response.json(); break; // here response body is "unstreamed" and passed as promise - parses JSON response into native Javascript objects 
                case 'text/plain':       return response.text(); break;
            }
            
        }
        return response; // OK=false
    }.bind(this))
    .then(function (response){ 
        /* stage 2 - response payload Promise is resolved */
        // log('2', this.requestId);

        if (response===null) return null; // because of timeout

        if (typeof response.ok==='undefined') {
            this.response.responseData = response;
            if (this.response.responseData.error) onError.call(this, this.response.responseData.error); 
            else                                  onSuccess.call(this);
            this.saveUrl(url, this.method); // saves url in localStorage
        } else {
            this.response.responseError = this.getMetaData(response);
            onError.call(this, this.response.responseError);
        }

        // this.spinner('off');
        if(typeof this.onEnd==='function') this.onEnd(null, this.requestId); // used for stopping spinner, do not use first arg

        this.dispose();

        //////////////////////////// helpers //////////////////
        function onSuccess(){
            if (typeof this.onSuccess === 'function') this.onSuccess(this.response);
            else                                      this.showResult(this.response, {data:true});
        }
        function onError(err){
            if (typeof this.onError === 'function') this.onError(err, this.response);
            else                                    this.showResult( this.response , {err:true});
        }
    }.bind(this)) // comment .catch IDIOTENKODE below to see where errors are happening:  TODO: uncomment for prod
    // .catch(function(error) {
    //     // when network status is '(failed)' this will catch it. Also this will catch error not related to http request
    //     console.error('alfa-fetch', url, 'error:', error.message, 'timeOut.expired:',this.timeOut.expired);
        
    //     if(this.timeOut.expired) {return;} // ignore, timeout has occurred
    //     this.timeOut.end();
        
    //     this.response.responseError = {responseError: true, src:'alfa-fetch .catch()', message: error.message, url, requestId: this.requestId}; 

    //     if (typeof this.onError === 'function') this.onError(this.response.responseError, this.response);
    //     else this.showResult(this.response , {err:true}); 

    //     this.dispose();
    // }.bind(this));

};


AlfaFetch.prototype.timeOutConstructor = function(){
    // var that = this;
    var me = {
        start: function(){
            me.timer = + new Date();
            me.timeout = this.timeout;
            me.url = this.url;
            me.requestId = this.requestId;

            me.expired = false;
            me.isDone = false;
            me.onTimeout = this.onTimeout.bind(this);

            setTimeout(function(){
                if (!me.isDone) {
                    me.expired = true;
                    me.onTimeout();
                }
            }, me.timeout);
        },
        end: function(){
            me.timer = (+ new Date()) - me.timer; // elapsed time

            if (me.expired) {
                console.warn(`[alfa-fetch es6] timeout ERR: response received after ${(me.timer/1000)}s and IGNORED because timeout ${me.timeout/1000}s is exceeded
request  ID: ${me.requestId}
request url: ${me.url}`);
                // this.response.responseError = {message}; // NOTE: too late, timer callback already called
                return false;
            } else {
                log('timeout OK: response received after', me.timer/1000, 's, before timeout', me.timeout/1000, 's');
                me.isDone = true;
                return true;
            }
        }
    }; 
    return me;
};
AlfaFetch.prototype.onTimeout = function(){
    log('ERR: onTimeout()', this.timeout);
    this.response.responseError = {message: `timeout ${this.timeout/1000}s`, requestId: this.requestId, url: this.url};
    if (typeof this.onError === 'function') this.onError(this.response.responseError);
    else this.showResult(this.response , {err:true});
    this.dispose();
};

AlfaFetch.prototype.spinner = function(mode){

    var spinnerWidget = window.alfaModalSpinner;
    if(!spinnerWidget) return; // --- >

    if(mode==='on'){
        this.spinnerNr = spinnerWidget.on();
    } else if(mode==='off'){
        spinnerWidget.off(this.spinnerNr );
    }
};
AlfaFetch.prototype.saveUrl = function(url, method){
    // window.localStorage.setItem('alfa-fetch-last-successfull-call', {method, url});
    window.localStorage.setItem('alfa-fetch-last-successfull-url', url);
    window.localStorage.setItem('alfa-fetch-last-successfull-method', method);
};

/** built in debugger:  */
AlfaFetch.prototype.showResult = function(data, opts){
    opts = opts || {};
    // log('3', this.requestId, opts);

    var div = opts.err ? document.getElementById(this.errorElementId) : document.getElementById(this.responseElementId);   
    
    switch (this.contentType){
        case 'application/json': data = JSON.stringify(data, null, 4); break
        case 'text/plain':        break;
    }
    // data = JSON.stringify(data, null, 4);

    if (div) div.innerHTML=`<pre>${data}</pre>`;
};

AlfaFetch.prototype.getMetaData = function(response){
    // https://developer.mozilla.org/en-US/docs/Web/API/Response
    return {
        ok:         response.ok, 
        status:     response.status,
        statusText: response.statusText,
        type:       response.type,
        url:        response.url,
        useFinalURL: response.useFinalURL,
        headers:    this.getHeaders(response.headers),
        redirected: response.redirected,
        body:       response.body
    };
};
AlfaFetch.prototype.getHeaders = function(headers){
    if (!headers) return null;
    var h={};
    for (var pair of headers.entries()){
        h[pair[0]] = pair[1];
    }
    return h;
};
/** long unique string */
AlfaFetch.prototype.uuid = function () { 
    return (function b(a) { 
        return a ? (a ^ Math.random() * 16 >> a / 4).toString(16) : ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, b); 
    })(); 
};
/** short unique string */
AlfaFetch.prototype.uid = function () {
    return Math.random().toString(36).split('').filter( function(value, index, self) { 
        return self.indexOf(value) === index;
    }).join('').substr(2,8);
};
AlfaFetch.prototype.dispose = function(){
    // log('X', this.requestId);
    this.spinner('off');
    delete alfaFetchInstances[this.requestId];
    // log('Y', this.requestId, 'AFTER DELETING this instance !');
};



/**  helper */
function log(){
    return;
    var args = Array.prototype.slice.call(arguments); 
    args.unshift('%c[alfa-fetch es6]', 'color:cyan');
    console.log.apply(null, args);
}