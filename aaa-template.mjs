/** example */

var aaaTempl = {
    es6module: 'aaa-template.mjs',
    message: 'export default'
}

/** 'export' keyword MUST be at least 4 characters after NewLine character ASCI 10 (to allow one tab sapce) */

 export default aaaTempl;

/** REQUIRE-es6module.js replaces
 * export default aaaTempl;
 * to
 * module.exports = aaaTemplate
 */

 /** placing 'export' string close to left margin is allowed in comments
  * export
  */
 //export

 /** placing 'export' string close to left margin is allowed  */
 'export'