/*! copyright 2018 alfalabs.net license MIT
 * responds to user commands from menus, buttons etc
 * 
 *  runs action in current application <page element> instance
 */

import AlfaConsoleLog from './alfa-console-log.mjs'; 
var alfaConsoleLog = new AlfaConsoleLog('alfa-action-mamager')
var log =     alfaConsoleLog.log.bind(alfaConsoleLog)
var logWarn = alfaConsoleLog.logWarn.bind(alfaConsoleLog)
var logErr =  alfaConsoleLog.logErr.bind(alfaConsoleLog)


/** constructor 
 * 
 *  @param {Object} options - added to this.cfg
 */
function AlfaActionManager(options){

    var defaults = {
        author: 'alfalabs.net',
        // name: 'AlfaActionManager'
    }
    this.setCfgValues(options, defaults) 
     
}
/** get configuration from defauls, options  */
AlfaActionManager.prototype.setCfgValues = function(options, defaults){
    options = options || {}
    this.cfg = Object.assign({}, defaults, options)
}


/** usually handles click on alfa-menu
 * @param args = {itemIndex, item, id}
 */
AlfaActionManager.prototype.action = function(action, opts){
    // DONE before calling this fn: alfaDrawerRight.close()

    opts  = opts || {}

    /** action is required in item.payload */
    if(action){}else{logErr('alfaActionManager.action()', 'action is missing!');  return;}

    var itemIndex = opts.itemIndex || null
    log({itemIndex}, 'alfaActionManager.action() run action:', action)
    

    var currentPage; // = this.getCurentPage();

    /** actions in Polymer alfa-autopage as currentPage
     *  window.navCallbacks - from Node_API\WWW\alfa-nav-polymer
     */
    if(window.navCallbacks){
        currentPage = window.navCallbacks.getCurrentPage()
        currentPage.run_action_or_actions(action)
    }

    /** actions in new Polymer pages
     *  window.alfaPagesNavigation - from ALFA_Polymer_components\alfa-pages-navigation 
     */
    if(window.alfaPagesNavigation){

        /** is this applet action or page action ? */
        
        /** applet actions */
        if(action.goto_page){
            window.alfaPagesNavigation.gotoPage(action.goto_page)
            return;
        }

        /** page actions */
        currentPage = window.alfaPagesNavigation.getCurrentPage()
        currentPage.runAction(action)
    }
}

/** usually handles click on alfa-menu (called from index.html, where alfa-menu is created)
 * @param args = {itemIndex, item, id}
 */
AlfaActionManager.prototype.menuClickHandler = function(args){
    // DONE before calling this fn: alfaDrawerRight.close()

    /** action is required in item.payload */
    if(args.item.payload && args.item.payload.action){}else{logErr('alfaActionManager.menuClickHandler()', 'action is missing!', args);  return;}

    var action = args.item.payload.action;

    this.action(action, {itemIndex: args.itemIndex, item: args.item})
   
    return; ///////////////////////////////////////////////////////////////////////////////////////

    // MOVED to this.action()
    log(args.itemIndex, 'alfaActionManager.menuClickHandler() run action:', action)
    

    var currentPage; // = this.getCurentPage();

    /** actions in Polymer alfa-autopage as currentPage
     *  window.navCallbacks - from Node_API\WWW\alfa-nav-polymer
     */
    if(window.navCallbacks){
        currentPage = window.navCallbacks.getCurrentPage()
        currentPage.run_action_or_actions(action)
    }

    /** actions in new Polymer pages
     *  window.alfaPagesNavigation - from ALFA_Polymer_components\alfa-pages-navigation 
     */
    if(window.alfaPagesNavigation){

        /** is this applet action or page action ? */
        
        /** applet actions */
        if(action.goto_page){
            window.alfaPagesNavigation.gotoPage(action.goto_page)
            return;
        }

        /** page actions */
        currentPage = window.alfaPagesNavigation.getCurrentPage()
        currentPage.runAction(action)
    }
}

/** gets current "app page" element instance of "single webpage" app, depending on environment  */
AlfaActionManager.prototype.getCurentPage = function(){
    
    /** actions in Polymer alfa-autopage as currentPage
     *  window.navCallbacks - from Node_API\WWW\alfa-nav-polymer
     */
    if(window.navCallbacks){
        return window.navCallbacks.getCurrentPage()
    }

    /** actions in new Polymer pages
     *  window.alfaPagesNavigation - from ALFA_Polymer_components\alfa-pages-navigation 
     */
    if(window.alfaPagesNavigation){
        return window.alfaPagesNavigation.getCurrentPage()
    }
}



export default AlfaActionManager;

