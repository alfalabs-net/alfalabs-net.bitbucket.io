/*! alfa-data-table-ui  Copyright (c) 2020 alfalabs.net license MIT */
/**  
 *  without databinding
*/

const $dqs = document.querySelector.bind(document)
const $dce = document.createElement.bind(document)

class AlfaDataTableUI{

    constructor(containerSelector){
        this.container =  $dqs(containerSelector)
        // this.container.innerHTML = "Hi dere!"

        this.table = $dce('table')
        this.container.append(this.table)
    }

    load(tbl){
        this.make_html_table(this.table, tbl)
    }

    make_html_table(tableElem, list){
   
        clearNode(tableElem) // tableElem.innerHTML = ''
    
        var tbody = $dce('tbody')
        list.forEach(function(row, i){
            var colName
            var tr = $dce('tr')
            tr.setAttribute('row-index', i)
            for(colName in row){
                var td = $dce('td')
                td.innerHTML = row[colName]
                tr.append(td)
            }
            tr.addEventListener('click', this.onTblRowClick.bind(this))
            tbody.append(tr)
        }.bind(this))
        tableElem.append(tbody)
    }

    onTblRowClick(ev){
        var tr = ev.target.closest('tr');
        var index = tr.getAttribute('row-index')
        console.log('onTblRowClick', index)
    }
}

export {AlfaDataTableUI} // --- ----------------- >


function clearNode(myNode){
    while (myNode.firstChild) {
        myNode.removeChild(myNode.lastChild);
    }
}