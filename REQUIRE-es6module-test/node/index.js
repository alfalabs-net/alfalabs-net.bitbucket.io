
const requireEs6 = require('../../REQUIRE-es6module'); /** usage of es6 module from ALFA_es6_and_node_modules */

var MODULE_NAME = 'REQUIRE-es6module';

module.exports = function(req, res){
    // console.log(`[${MODULE_NAME}] index.js`)
    var RESULT = {
        MODULE_NAME,
        info: 'server side index.js module',
    }

    var coreCallbacks = require('../../_node-server/core-callbacks')(req, res , {MODULE_NAME});


     /** replace testing code:    */


    switch (req.params.cmd){
        case '1':   load1(); break;
        case '2':   load2(); break;
        case '3':   load3(); break;

        case '4':   load4(); break;
        case 'exp': loadExp(); break;
        case 'imp': loadImp(); break;

        case 'err': loadE(); break;
        case 'notexist': loadNotExist(); break
        default: defaultResponse();
    }

    // new
    function load4(){
        /** es6module with: export default */
        var es6module1 = requireEs6('./REQUIRE-es6module-test/export-obj.mjs');
        RESULT.requiring = 'export-obj.mjs';
        RESULT.es6module1 = es6module1; 
        coreCallbacks.sendResutl(RESULT);
    }
    function loadExp(){
        /** es6module with: export default */
        var es6module1 = requireEs6('./REQUIRE-es6module-test/test-export.mjs');
        RESULT.requiring = 'test-export.mjs';
        RESULT.es6module1 = es6module1; 
        coreCallbacks.sendResutl(RESULT);
    }
    function loadImp(){
        /** es6module with: export default */
        var es6module1 = requireEs6('./REQUIRE-es6module-test/test-import.mjs', {requireStr: 'require'});
        RESULT.requiring = 'test-import.mjs';
        RESULT.es6module1 = es6module1; 
        coreCallbacks.sendResutl(RESULT);
    }


    // old
    function load1(){
        /** es6module with: export default */
        var es6module1 = requireEs6('./aaa-template.mjs');
        RESULT.requiring = 'aaa-template.mjs';
        RESULT.es6module1 = es6module1; 
        coreCallbacks.sendResutl(RESULT);
    }

    function load2(){
        /** es6module with: export {object} */
        var es6module2 = requireEs6('./REQUIRE-es6module-test/export-obj.mjs');
        RESULT.requiring = 'export-obj.mjs';
        RESULT.es6module2 = es6module2; 
        coreCallbacks.sendResutl(RESULT);
    }

    function load3(){
        /** es6module with: export {object} */
        var es6module3 = requireEs6('./REQUIRE-es6module-test/export-nested.mjs');
        RESULT.requiring = 'export-nested.mjs';
        RESULT.es6module3 = es6module3; 
        coreCallbacks.sendResutl(RESULT);
    }

    function loadE(){
        /** es6module with error */
        var es6module3 = requireEs6('./REQUIRE-es6module-test/export-with-err.mjs');
        RESULT.requiring = 'export-with-err.mjs';
        RESULT.es6module3 = es6module3; 
        coreCallbacks.sendResutl(RESULT);
    }
    function loadNotExist(){
        /** es6module with error */
        var es6module = requireEs6('./REQUIRE-es6module-test/xxx-xxx.mjs');
        RESULT.requiring = 'non existing file';
        RESULT.es6module = es6module; 
        coreCallbacks.sendResutl(RESULT);
    }

    function defaultResponse(){
        RESULT.error = {message:'cmd unknown.', params: req.params}
        coreCallbacks.sendResutl(RESULT)
    }
 
   
}