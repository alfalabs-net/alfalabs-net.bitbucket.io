/** cause ERROR for testing */
var obj1 = {
    es6module: 'export-with-err.mjs',
    message: 'export {obj1}'
}

var obj2 = {
    es6module: 'export-with-err.mjs',
    message: 'export {obj2}'
}

/** this line will cause failed conversion, it should be: export {obj1, obj2}; */
module.exports = {obj1, obj2};

