/**
 *      1. inject: 
 *      const requireEs6 = require('../../REQUIRE-es6module');
 * 
 *      2. convert
 *      import {obj1, obj2} from 'export-obj.mjs'
 *      to
 *      var {obj1, obj2} = REQUIRE-es6module('export-obj.mjs')
 */

// import {obj1, obj2} from 'export-obj.mjs' highlight the line, then  ctrl+alt+M runs import-to-require
import {obj1, obj2} from 'export-obj.mjs'

var obj3 = {
    es6module: 'export-nested.mjs',
    message: 'export {obj1} (new)'
}


// export {obj1, obj2, obj3}; ctrl+alt+P
export {obj1, obj2, obj3};

/** REQUIRE-es6module.js replaces above to
module.exports = {}
 */