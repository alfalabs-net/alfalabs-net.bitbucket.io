/** AlfaDragDropUI */

import AlfaConsoleLog from '../alfa-console-log.mjs'; 
var alfaConsoleLog = new AlfaConsoleLog('alfa-drag-drop-ui')
var log =     alfaConsoleLog.log.bind(alfaConsoleLog)
var logWarn = alfaConsoleLog.logWarn.bind(alfaConsoleLog)
var logErr =  alfaConsoleLog.logErr.bind(alfaConsoleLog)


const $dqs = document.querySelector.bind(document); 
const $dce = document.createElement.bind(document); 


/** constructor 
 * 
 *  @param {String} elementSelector - a CSS selector, unique for element to create
 *  @param {Object} options - added to this.cfg, (also cfg optinos can be html attributes, they will be camel capitalized)
 */
function AlfaDragDropUI(elementSelector, options){

    var defaults = {
        author: 'alfalabs.net',
        moduleName: 'AlfaDragDropUI'
    }
    this.cfg = Object.assign({}, defaults, options)

    /** ui element has an element on page */
    this.elementSelector = elementSelector;
    
    this.container =  $dqs(elementSelector)
    alfaConsoleLog.setCfg({id: this.container.getAttribute('id')})
    log('constructor()')

    this.list_files = []

    this.createElement()
}

AlfaDragDropUI.prototype.changeCfg = function(cfg){
    this.cfg = Object.assign( this.cfg, cfg)
}

AlfaDragDropUI.prototype.createElement = function(){


    this.container.addEventListener('dragover', function(ev){
        ev.preventDefault()
    })

    this.container.addEventListener('drop', function(ev){
        ev.preventDefault()

        this.current_drop_pre =  $dce('pre')
        this.container.append(this.current_drop_pre)

        const items = Array.from(ev.dataTransfer.items)
        this.addItems(items)

        // const images = files.filter(function(file){
        //     return file.type.startsWith('image/')
        // })

    }.bind(this))  
}
AlfaDragDropUI.prototype.addItems = function(items){

    items.forEach(function(item){

        item = item.webkitGetAsEntry();
        if(item){this.traverseFileTree(item);}

    }.bind(this))
   
}

AlfaDragDropUI.prototype.showItemLine = function(item){
    var itemLine = item.path +' '+item.name +'\n'
    this.current_drop_pre.innerHTML += itemLine
}

// async!
AlfaDragDropUI.prototype.traverseFileTree = function(item, path) {
    path = path || "";
    if (item.isFile) {
        // Get file
        item.file(function(file) {
            if(this.isFiltered(file)){
                file.path = path
                this.list_files.push(file) 
                this.showItemLine(file)
            }
        }.bind(this));
    } else if (item.isDirectory) {
        // Get folder contents
        var dirReader = item.createReader();
        dirReader.readEntries(function(entries) {
            for (var i=0; i<entries.length; i++) {
                this.traverseFileTree(entries[i], path + item.name + "/");
            }
        }.bind(this));
    }
}
AlfaDragDropUI.prototype.isFiltered = function(file){
    if(!this.cfg.mimeType) {return true}
    return file.type.startsWith(this.cfg.mimeType +'/')
}

AlfaDragDropUI.prototype.makeHrefLinks = function(containerElemSelector){
    var ul = $dce('ul')
    this.list_files.forEach(function(item){
        var li = $dce('li')
        var a = $dce('a')
        var href = `${item.path}/${item.name}`
        a.setAttribute('href', href)
        a.setAttribute('target', '_blank')
        a.innerText=item.name
        li.append(a)
        ul.append(li)
    })
    $dqs(containerElemSelector).append(ul)
}

AlfaDragDropUI.prototype.clear = function(){
    this.clearNode(this.container)
}
AlfaDragDropUI.prototype.clearNode = function(myNode){
    while (myNode.firstChild) {
        myNode.removeChild(myNode.lastChild);
    }
}

export default AlfaDragDropUI;
